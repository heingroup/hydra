from hein_robots.kinova.kinova_sequence import KinovaSequence

'''
This is the sequence files extraction for translating the same level directory files
Use full filepath
'''

vial_tray = KinovaSequence(r'D:\git_repositories\Direct_inject_N9\configuration\sequences\vial_tray.json')
stir_plate = KinovaSequence(r'D:\git_repositories\Direct_inject_N9\configuration\sequences\stir_plate.json')
sm_locations = KinovaSequence(r'D:\git_repositories\Direct_inject_N9\configuration\sequences\sm_locations.json')
regripping_action = KinovaSequence(
    r'D:\git_repositories\Direct_inject_N9\configuration\sequences\regripping_action.json')
dosing_location = KinovaSequence(
    r'D:\git_repositories\Direct_inject_N9\configuration\sequences\dosing_location.json')
gateway_moves = KinovaSequence(
    r'D:\git_repositories\Direct_inject_N9\configuration\sequences\gateway_moves.json')
quantos_ = KinovaSequence(r'D:\git_repositories\Direct_inject_N9\configuration\sequences\quantos.json')
pour_action = KinovaSequence(r'D:\git_repositories\Direct_inject_N9\configuration\sequences\pour_action.json')
cap = KinovaSequence(r'D:\git_repositories\Direct_inject_N9\configuration\sequences\cap.json')
ventilator = KinovaSequence(r'D:\git_repositories\Direct_inject_N9\configuration\sequences\ventilator.json')
centrifuge = KinovaSequence(r'D:\git_repositories\Direct_inject_N9\configuration\sequences\centrifuge.json')

sequences = [vial_tray, stir_plate, sm_locations, regripping_action, dosing_location, gateway_moves, quantos_,
             pour_action, cap, ventilator,centrifuge]

sequence_names = ['vial_tray', 'stir_plate', 'sm_locations', 'regripping_action', 'dosing_location', 'gateway_moves',
                  'quantos_', 'pour_action', 'cap', "ventilator",'centrifuge']