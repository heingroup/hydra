import re

from hein_utilities.control_manager import do_nothing
from hein_utilities.slack_integration.bots import WebClientOverride
from hein_utilities.slack_integration.parsing import ignore_bot_users
from hein_utilities.slack_integration.slack_managers import RTMControlManager

bot_token = None  # bot token of slack
bot_name = 'Bot'
channel_name = '#Channel'

bot = RTMControlManager(
        token=bot_token,
        channel_name=channel_name,
        start_action=do_nothing,
        stop_action=do_nothing,
        status_query=do_nothing,
        pause_action=do_nothing,
        resume_action=do_nothing,
    )

