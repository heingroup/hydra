from typing import Any

import time

from ftdi_serial import Serial
from ika import MagneticStirrer
from integrity.integrity.api import Integrity10
from mtbalance import ArduinoAugmentedQuantos
from north_devices.pumps.tecan_cavro import TecanCavro
from vapourtec.vapourtec import SF10
from vicivalve import VICI

from experiments.relayboard import RelayBoard
from sampleomatic_stepper.sampleomatic_stepper_api import SampleomaticStepper

VAPORTECH_TO_THF = 2


def row_column_to_position_integrity(row, column):
    if column == 1:
        return 11 - row
    else:
        return 6 - row


class SolidLiquidHandlingManager:
    def __init__(self, quantos: ArduinoAugmentedQuantos, sm: SampleomaticStepper,
                 sample_valve: Any, hplc_valve: VICI, selection_valve: VICI,
                 sample_pump: TecanCavro, push_pump: TecanCavro,
                 vapourtec: SF10,
                 stirring_device: Integrity10 or MagneticStirrer,
                 relays: RelayBoard):

        self.TecanCavro = TecanCavro
        self.quantos = quantos
        self.sm = sm
        self.sample_valve = sample_valve
        self.hplc_valve = hplc_valve
        self.vici10 = selection_valve
        self.sample_pump = sample_pump
        self.push_pump = push_pump
        self.sf10 = vapourtec
        self.relays = relays
        self.stirring_device = stirring_device
        self.is_washed = False

        self.SAMPLE_PUMP_VOL = 5  # mL
        self.PUSH_PUMP_VOL = 2.5  # mL
        self.DOSING_PUMP_VOL = 1  # mL
        self.AIR_VOLUME = 1.3  # ml air needed to transfer sample from sampleomatic head to sample loop
        self.SECOND_AIRGAP_TRANSFER = 0.085  # ml
        self.SAMPLE_INLINE = "A"
        self.SAMPLE_BYPASS = "B"
        self.HPLC_FILL = "A"
        self.HPLC_INJECT = "B"
        self.LOOP_A_INJECT = "A"
        self.LOOP_A_LOAD = "B"
        self.LOOP_B_INJECT = "B"
        self.LOOP_B_LOAD = "A"

        # flow rates
        self.STOCK_DRAW_RATE = 2
        self.SAMPLE_DRAW_RATE = 1  # mL/min
        self.SOLVENT_DRAW_RATE = 5  # mL/min
        self.SOLVENT_PUSH_RATE = 1.0  # mL/min
        self.SOLVENT_DRAW_RATE_PPUMP = 3  # mL/min push pump
        self.SOLVENT_PUSH_RATE_PPUMP = 1  # mL/min  push pump
        self.SAMPLE_FLUSH_RATE = 3  # mL/min
        self.PUSH_RATE_FINAL = 0.3

        # flush volumes
        self.SAMPLE_FLUSH_VOLUME = self.SAMPLE_PUMP_VOL  # mL
        self.PUSH_FLUSH_VOLUME = self.PUSH_PUMP_VOL  # mL

        self.SYRINGE_PUMP_CALIBRATION_FACTOR = 1.023
        self.PUMP_VELOCITY_FAST = 20  # mL/min
        self.PUMP_VELOCITY_DEFAULT = 10
        self.PUMP_VELOCITY_SLOW = 2
        self.SM_MOVE_WITHDRAW = -80
        self.SM_MOVE_DISPENSE = -40

        self.SM_MOVE_WASH = -15
        self.SM_MOVE_CENTRIFUGE_DISPENSE = -15
        self.SM_MOVE_HPLC_WITHDRAW = -44
        self.SM_MOVE_TALL_WITHDRAW = -95
        # self.SM_MOVE_TALL_WITHDRAW = -112
        self.SM_MOVE_CENTRIFUGE_WITHDRAW = -56
        self.QUANTOS_STEP_INTEGRITY_VIAL = 2750
        self.VAPOURTEC_TO_ETHANOL = 1
        self.VAPOURTEC_TO_THF = 2
        self.VAPOURTEC_TO_CDI = 3
        self.VAPOURTEC_TO_DCM = 4
        self.VAPOURTEC_TO_BENZENE = 5
        self.VAPOURTEC_TO_AIR = 9
        self.VAPOURTEC_TO_FILTER_BEAKER = 10
        # ports on the Cavro pumps
        self.SAMPLE_PORT = 3
        self.SOLVENT_PORT = 2
        self.AIR_PORT = 1
        self.SM_MOVE_ADJUSTMENT = -12
        self.adjust()
        self.TRAVEL_LENGTH = 0.94
        self.SECOND_TRAVEL_LENGTH = -0.31

    def adjust(self):
        """
        When switching to a new needle
        """
        self.SM_MOVE_WITHDRAW -= self.SM_MOVE_ADJUSTMENT
        self.SM_MOVE_DISPENSE -= self.SM_MOVE_ADJUSTMENT

        self.SM_MOVE_WASH -= self.SM_MOVE_ADJUSTMENT
        self.SM_MOVE_CENTRIFUGE_DISPENSE -= self.SM_MOVE_ADJUSTMENT
        self.SM_MOVE_HPLC_WITHDRAW -= self.SM_MOVE_ADJUSTMENT
        self.SM_MOVE_TALL_WITHDRAW -= self.SM_MOVE_ADJUSTMENT
        self.SM_MOVE_CENTRIFUGE_WITHDRAW -= self.SM_MOVE_ADJUSTMENT

    def wash(self, volume: float = 3, solvent: int = VAPORTECH_TO_THF):
        # self.sample_valve.switch_valve(self.SAMPLE_BYPASS)
        self.sm.home()
        self.sm.move_needle(self.SM_MOVE_WASH)
        self.vici10.switch_valve(solvent)
        self.pump_by_vaportech(volume)
        self.sm.move_needle(-self.SM_MOVE_WASH)
        self.is_washed = True

    def wash_by_syringe_pump(self, volume: float = 6):
        self.bypass_setup()
        self.sm.home()
        self.sm.move_needle(self.SM_MOVE_WASH)
        self.sample_pump.dispense_ml(volume, 1, 3, velocity_ml=self.PUMP_VELOCITY_DEFAULT)
        self.sm.move_needle(-self.SM_MOVE_WASH)
        self.is_washed = True

    def wash_push_line(self):
        self.push_pump.switch_valve(self.SOLVENT_PORT)
        self.push_pump.move_absolute_ml(self.PUSH_PUMP_VOL, self.SOLVENT_DRAW_RATE_PPUMP)
        time.sleep(4)
        self.push_pump.switch_valve(self.SAMPLE_PORT)
        self.push_pump.move_absolute_ml(0, velocity_ml=self.SOLVENT_PUSH_RATE, wait=False)

    def pump_by_vaportech(self, volume: float):
        self.relays.set_output(1, True)
        self.sf10.set_mode(self.sf10.MODE_DOSE)
        self.sample_valve.switch_valve('B')
        self.sf10.set_flow_rate(10)  # mL/min
        self.sf10.set_dose_volume(volume)  # mL
        time.sleep(0.5)
        self.sf10.start()
        time.sleep(volume / 10 * 60 + 2)
        self.relays.set_output(1, False)

    def bypass_setup(self):
        self.sample_valve.switch_valve(self.SAMPLE_BYPASS)
        self.sample_valve.switch_valve(self.HPLC_INJECT)

    def stop_stirring(self, position: int = 10, rate: int = 0):
        self.stirring_device.change_stir_speed(position, rate)

    def start_stirring(self, position: int = 10, rate: int = 500):
        self.stirring_device.change_stir_speed(position, rate)

    def start_stirring_ika(self, rate: int = 500):
        self.stirring_device.target_stir_rate = rate
        self.stirring_device.start_stirring()

    def stop_stirring_ika(self):
        self.stirring_device.stop_stirring()

    def stop_ika_until_temperature(self, temperature):
        while self.stirring_device.probe_temperature <= temperature:
            time.sleep(1)
        self.stirring_device.stop_stirring()

    def start_heating_ika(self, temperature: float = 25):
        self.stirring_device.target_temperature = temperature
        self.stirring_device.start_heating()
        import threading
        threading.Thread(target=self.stop_ika_until_temperature, args=[temperature*0.75]).start()

    def stop_heating_ika(self):
        self.stirring_device.stop_heating()

    def change_temperature(self, position: int = 10, temp: float = 25):
        self.stirring_device.change_temperature(position, temp)

    def sonicator_on(self):
        self.sm.arduino.send_string("RON 1")

    def sonicator_off(self):
        self.sm.arduino.send_string("ROF 1")

    def row_column_to_position_integrity(self, row, column):
        if column == 1:
            return 11 - row
        else:
            return 6 - row

    def position_to_row_column_integrity(self, position):
        if position > 5:
            return 11 - position, 1
        else:
            return 6 - position, 2

    def position_to_row_column_ika(self, position):
        if position > 6:
            return 3, position - 6
        elif position > 3:
            return 2, position - 3
        else:
            return 1, position
