from configuration import deck
from configuration.arm_manager.arm_manager import ArmManager
from configuration.kinova_deck_manager.kinova_deck_manager import KinovaDeckManager
from configuration.liquid_handling_manager.solid_liquid_handling_manager import SolidLiquidHandlingManager

kinova_manager = ArmManager(arm=deck.arm, action_sequences=deck.sequences, sequence_names=deck.sequence_names)
solid_liquid_handling_manager = SolidLiquidHandlingManager(quantos=deck.quantos,
                                                           sm=deck.sm,
                                                           sample_valve=deck.sample_valve,
                                                           hplc_valve=deck.hplc_valve,
                                                           selection_valve=deck.vici10,
                                                           sample_pump=deck.sample_pump,
                                                           push_pump=deck.push_pump,
                                                           vapourtec=deck.sf10,
                                                           integrity=deck.integrity,
                                                           relays=deck.relays
                                                           )
kinova_experiment = KinovaDeckManager(kinova_manager, solid_liquid_handling_manager)
