import datetime
import os
import threading
from typing import Optional

import time

from configuration import deck
from configuration.arm_manager.arm_manager import ArmManager
from configuration.liquid_handling_manager.solid_liquid_handling_manager import SolidLiquidHandlingManager, \
    VAPORTECH_TO_THF
# from configuration.lucky import lucky
from configuration.bot import bot
from experiments.utilities.hplc_pump_control.hplc_pump_control import turn_on_pump


class KinovaDeckManager:
    def __init__(self, kinova_manager: ArmManager, solid_liquid_handling_manager: SolidLiquidHandlingManager):
        self.arm_manager = kinova_manager
        self.solid_liquid_handling_manager = solid_liquid_handling_manager
        self.hplc_injection_time = datetime.datetime.now()

    def dosing_head_from_quantos(self):
        safe_out = self.arm_manager.action_sequences['dosing_location']['out_q']
        grab = self.arm_manager.action_sequences['dosing_location']['grab_q']
        lift = self.arm_manager.action_sequences['dosing_location']['lift_q']
        safe_h_side = self.arm_manager.action_sequences['regripping_action']['safe_pose_h_side']

        self.arm_manager.arm.move_to_locations(safe_h_side)
        self.arm_manager.arm.move_to_locations(safe_out)
        self.arm_manager.arm.open_gripper(0.40)
        self.arm_manager.arm.move_to_locations(grab)
        self.arm_manager.arm.open_gripper(0.71)
        time.sleep(2)
        self.arm_manager.arm.move_to_locations(lift)
        self.arm_manager.arm.move_to_locations(safe_out)
        self.arm_manager.arm.move_to_locations(safe_h_side)

    def dosing_head_to_position(self, pos: int = 1):

        # Not in use, template only
        safe_out_init = self.arm_manager.action_sequences['dosing_location']['out_1']
        safe_out = self.arm_manager.action_sequences['dosing_location']['out_' + str(pos)]
        grab = self.arm_manager.action_sequences['dosing_location']['grab_' + str(pos)]
        lift = self.arm_manager.action_sequences['dosing_location']['lift_' + str(pos)]
        safe_h_side = self.arm_manager.action_sequences['regripping_action']['safe_pose_h_side']
        safe_h_backward = self.arm_manager.action_sequences['regripping_action']['safe_pose_h_backward']

        self.arm_manager.arm.move_to_locations(safe_h_side)
        self.arm_manager.arm.move_to_locations(safe_h_backward)
        self.arm_manager.arm.move_to_locations(safe_out_init)
        self.arm_manager.arm.move_to_locations(safe_out)
        self.arm_manager.arm.move_to_locations(lift)
        self.arm_manager.arm.move_to_locations(grab)
        self.arm_manager.arm.open_gripper(0.4)
        self.arm_manager.arm.move_to_locations(safe_out)
        self.arm_manager.arm.move_to_locations(safe_h_backward)

    def sonicate_vial(self, seconds: float):
        # Use when vertically grabs an integrity vial
        sonicator = self.arm_manager.action_sequences['vial_tray']['sonicator']
        lift = sonicator.translate(z=100)
        safe_pose_v = self.arm_manager.action_sequences['regripping_action']['safe_pose_v']

        self.arm_manager.arm.move(z=100, relative=True, velocity=50)
        self.arm_manager.arm.move_to_locations(safe_pose_v)
        self.arm_manager.arm.move_to_locations(lift)
        self.arm_manager.arm.move_to_locations(sonicator)
        self.solid_liquid_handling_manager.sonicator_on()
        time.sleep(seconds)
        self.solid_liquid_handling_manager.sonicator_off()
        self.arm_manager.arm.move_to_locations(lift)
        self.arm_manager.arm.move_to_locations(safe_pose_v)

    def rotation_sonication(self, rotation: int = 5):
        """
        Start/end holding the capped vial
        """
        sonicator_tilted = self.arm_manager.action_sequences['vial_tray']['sonicator_tilted']
        sonicator_tilted_lifted = sonicator_tilted.translate(z=200)
        sonicator_rotate_1 = self.arm_manager.action_sequences['vial_tray']['sonicator_rotate_1']
        sonicator_rotate_2 = self.arm_manager.action_sequences['vial_tray']['sonicator_rotate_2']
        safe_pose_v = self.arm_manager.action_sequences['regripping_action']['safe_pose_v']
        #
        self.arm_manager.arm.move_to_locations(safe_pose_v)
        self.arm_manager.arm.move_to_locations(sonicator_tilted_lifted)
        self.arm_manager.arm.move_to_locations(sonicator_tilted)
        self.solid_liquid_handling_manager.sonicator_on()
        for i in range(5):
            self.arm_manager.arm.move_to_locations(sonicator_tilted, sonicator_rotate_1, sonicator_rotate_2,
                                                   sonicator_tilted)
        self.solid_liquid_handling_manager.sonicator_off()

        self.arm_manager.arm.move_to_locations(sonicator_tilted_lifted)
        self.arm_manager.arm.move_to_locations(safe_pose_v)

    def dispense_solvent(self, solvent: int = VAPORTECH_TO_THF, volume: float = 3, vial_holder='integrity',
                         row: int = 1, column: int = 1):
        self.solid_liquid_handling_manager.wash(5, solvent=solvent)
        self.arm_manager.sm_from_base()
        if vial_holder == 'integrity':
            self.arm_manager.sm_to_integrity(row, column)
        elif vial_holder == 'ika':
            self.arm_manager.sm_to_ika(row, column)
        time.sleep(1)
        self.solid_liquid_handling_manager.sm.home()
        self.solid_liquid_handling_manager.sm.move_needle(self.solid_liquid_handling_manager.SM_MOVE_DISPENSE)
        self.solid_liquid_handling_manager.vici10.switch_valve(solvent)
        self.solid_liquid_handling_manager.pump_by_vaportech(volume)
        time.sleep(1)
        self.solid_liquid_handling_manager.sm.move_needle(-self.solid_liquid_handling_manager.SM_MOVE_DISPENSE)
        self.solid_liquid_handling_manager.sm.home()
        self.arm_manager.sm_return_safe_pose_v()
        self.arm_manager.sm_to_base()

    def dose_reagent(self, reagent_row: int = 1, reagent_column: int = 1, reactor_row: int = 1, reactor_column: int = 1,
                     vial_holder = 'ika', volume: float = 1, is_cdi: bool = False):

        left_volume = volume
        self.solid_liquid_handling_manager.sample_valve.switch_valve('B')
        if is_cdi:
            self.solid_liquid_handling_manager.sample_pump.dispense_ml(5, 3, 2,
                                                                       self.solid_liquid_handling_manager.PUMP_VELOCITY_DEFAULT)
            self.solid_liquid_handling_manager.sm.move_needle(self.solid_liquid_handling_manager.SM_MOVE_DISPENSE)
            self.solid_liquid_handling_manager.vici10.switch_valve(self.solid_liquid_handling_manager.VAPOURTEC_TO_CDI)
            self.solid_liquid_handling_manager.pump_by_vaportech(4)
            self.solid_liquid_handling_manager.sm.move_needle(-self.solid_liquid_handling_manager.SM_MOVE_DISPENSE)
        # empty the dosing tube

        if is_cdi:
            self.dose_ml(reagent_row, reagent_column, reactor_row, reactor_column, volume=volume,vial_holder=vial_holder, is_cdi=is_cdi)
            return

        while left_volume > 0:
            if left_volume >= 1:
                self.dose_ml(reagent_row, reagent_column, reactor_row, reactor_column,vial_holder=vial_holder, volume=1)
                left_volume -= 1
            else:
                self.dose_ml(reagent_row, reagent_column, reactor_row, reactor_column,vial_holder=vial_holder,
                             volume=left_volume)
                left_volume -= left_volume
        self.arm_manager.arm.home()
        self.solid_liquid_handling_manager.wash(5)

    def dose_ml_from_tall_vials(self, reagent_row: int = 1, reagent_column: int = 1, reactor_row: int = 1,
                                reactor_column: int = 1, vial_holder = 'integrity',
                                volume: float = 1, duration: Optional = None):

        """
        Dose from tray A using integrity size vials, duration: in seconds
        """
        assert volume <= 4
        self.solid_liquid_handling_manager.sm.home()
        self.solid_liquid_handling_manager.wash_by_syringe_pump(5)
        self.arm_manager.sm_from_base()
        self.arm_manager.sm_to_tray_A(reagent_row, reagent_column)
        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(0.2, 3,
                                                                    self.solid_liquid_handling_manager.PUMP_VELOCITY_SLOW)
        self.solid_liquid_handling_manager.sm.move_needle(self.solid_liquid_handling_manager.SM_MOVE_TALL_WITHDRAW)
        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(volume *
                                                                    self.solid_liquid_handling_manager.SYRINGE_PUMP_CALIBRATION_FACTOR,
                                                                    3, 2)
        time.sleep(10)
        self.solid_liquid_handling_manager.sm.move_needle(-self.solid_liquid_handling_manager.SM_MOVE_TALL_WITHDRAW - 1)
        self.solid_liquid_handling_manager.sm.home()
        self.arm_manager.sm_from_reagent()
        # robot doses the liquid to the reactor
        if vial_holder == 'integrity':
            self.arm_manager.sm_to_integrity(reactor_row, reactor_column)
        elif vial_holder == 'ika':
            self.arm_manager.sm_to_ika(reactor_row, reactor_column)
        else:
            raise ValueError
        self.solid_liquid_handling_manager.sm.move_needle(self.solid_liquid_handling_manager.SM_MOVE_DISPENSE)
        if duration is None:
            self.solid_liquid_handling_manager.sample_pump.pump_to_ml(volume + 0.1, 3, 2)
        else:
            dose_time = 30
            for i in range(dose_time):
                self.solid_liquid_handling_manager.sample_pump.pump_to_ml((volume + 0.1) / dose_time, 3, 2)
                time.sleep(duration / dose_time)
        time.sleep(10)
        self.solid_liquid_handling_manager.sm.move_needle(-self.solid_liquid_handling_manager.SM_MOVE_DISPENSE)
        self.arm_manager.sm_return_safe_pose_v()
        self.arm_manager.sm_to_base()
        self.solid_liquid_handling_manager.wash_by_syringe_pump(2)

    def dose_ml_from_integrity(self, reagent_row: int = 1, reagent_column: int = 1, reactor_row: int = 1,
                               reactor_column: int = 1, volume: float = 1, duration: Optional = None):

        """
        Dose from tray A using integrity size vials, duration: in seconds
        """
        assert volume <= 4
        self.solid_liquid_handling_manager.sm.home()
        self.solid_liquid_handling_manager.wash_by_syringe_pump(5)
        self.arm_manager.sm_from_base()
        self.arm_manager.sm_to_integrity(reagent_row, reagent_column)
        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(0.2, 3,
                                                                    self.solid_liquid_handling_manager.PUMP_VELOCITY_SLOW)
        self.solid_liquid_handling_manager.sm.move_needle(self.solid_liquid_handling_manager.SM_MOVE_TALL_WITHDRAW)
        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(volume *
                                                                    self.solid_liquid_handling_manager.SYRINGE_PUMP_CALIBRATION_FACTOR,
                                                                    3, 2)
        time.sleep(10)
        self.solid_liquid_handling_manager.sm.move_needle(-self.solid_liquid_handling_manager.SM_MOVE_TALL_WITHDRAW - 1)
        self.solid_liquid_handling_manager.sm.home()
        self.arm_manager.sm_from_reagent()
        # robot doses the liquid to the reactor
        self.arm_manager.sm_to_integrity(reactor_row, reactor_column)
        self.solid_liquid_handling_manager.sm.move_needle(self.solid_liquid_handling_manager.SM_MOVE_DISPENSE)
        if duration is None:
            self.solid_liquid_handling_manager.sample_pump.pump_to_ml(volume + 0.1, 3, 2)
        else:
            dose_time = 30
            for i in range(dose_time):
                self.solid_liquid_handling_manager.sample_pump.pump_to_ml((volume + 0.1) / dose_time, 3, 2)
                time.sleep(duration / dose_time)
        time.sleep(10)
        self.solid_liquid_handling_manager.sm.move_needle(-self.solid_liquid_handling_manager.SM_MOVE_DISPENSE)
        self.arm_manager.sm_from_integrity()
        self.arm_manager.sm_to_base()

    # SPECIFICATION: volume <= 1
    def dose_ml(self, reagent_row: int = 1, reagent_column: int = 1, reactor_row: int = 1,
                reactor_column: int = 1,vial_holder = 'integrity', volume: float = 1, is_cdi: bool = False):
        # robot grabs the needle to reagent and take liquid
        self.solid_liquid_handling_manager.wash_by_syringe_pump(5)
        self.solid_liquid_handling_manager. \
            sample_pump.pump_from_ml(0.5, 3, self.solid_liquid_handling_manager.PUMP_VELOCITY_SLOW)
        self.arm_manager.sm_from_base()
        if not is_cdi:
            self.arm_manager.sm_to_reagent(reagent_row, reagent_column)
            self.solid_liquid_handling_manager.sm.move_needle(self.solid_liquid_handling_manager.SM_MOVE_WITHDRAW)
            self.solid_liquid_handling_manager.sample_pump.pump_from_ml(
                volume * self.solid_liquid_handling_manager.SYRINGE_PUMP_CALIBRATION_FACTOR, 3,
                self.solid_liquid_handling_manager.PUMP_VELOCITY_SLOW)

            time.sleep(20)

            self.solid_liquid_handling_manager.sm.move_needle(-self.solid_liquid_handling_manager.SM_MOVE_WITHDRAW)
            self.arm_manager.sm_from_reagent()

        # robot doses the liquid to the reactor
        if vial_holder == 'integrity':
            self.arm_manager.sm_to_integrity(reactor_row, reactor_column)
        elif vial_holder == 'ika':
            self.arm_manager.sm_to_ika(reactor_row,reactor_column)
        else:
            raise ValueError
        self.solid_liquid_handling_manager.sm.move_needle(self.solid_liquid_handling_manager.SM_MOVE_TALL_WITHDRAW)

        if is_cdi:
            self.solid_liquid_handling_manager.sample_valve.switch_valve('B')
            self.solid_liquid_handling_manager.vici10.switch_valve(self.solid_liquid_handling_manager.VAPOURTEC_TO_CDI)

            self.solid_liquid_handling_manager.pump_by_vaportech(volume)
        else:
            self.solid_liquid_handling_manager.sample_pump.pump_to_ml(
                volume * self.solid_liquid_handling_manager.SYRINGE_PUMP_CALIBRATION_FACTOR + 0.25, 3, 2)

        self.arm_manager.time_check_point = datetime.datetime.now()

        time.sleep(10)
        self.solid_liquid_handling_manager.sm.move_needle(-self.solid_liquid_handling_manager.SM_MOVE_TALL_WITHDRAW)
        self.arm_manager.sm_return_safe_pose_v()
        self.arm_manager.sm_to_base()

        # empty both pumps
        # self.kinova_manager.sample_pump.switch_valve(self.kinova_manager.SOLVENT_PORT)
        if not is_cdi:
            self.solid_liquid_handling_manager.sample_pump.home()
            self.solid_liquid_handling_manager.wash_by_syringe_pump(2)

    def dose_ml_to_capping(self, reagent_row: int = 1, reagent_column: int = 1, volume: float = 1):
        assert volume <= 2
        # robot grabs the needle to reagent and take liquid
        self.solid_liquid_handling_manager.wash_by_syringe_pump(5)

        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(
            0.5, 3, 2)
        self.arm_manager.sm_from_base()

        self.arm_manager.sm_to_reagent(reagent_row, reagent_column)
        self.solid_liquid_handling_manager.sm.move_needle(self.solid_liquid_handling_manager.SM_MOVE_WITHDRAW)
        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(
            volume * self.solid_liquid_handling_manager.SYRINGE_PUMP_CALIBRATION_FACTOR, 3, 2)
        time.sleep(20)

        self.solid_liquid_handling_manager.sm.move_needle(-self.solid_liquid_handling_manager.SM_MOVE_WITHDRAW)
        self.arm_manager.sm_from_reagent()

        # robot doses the liquid to the reactor
        self.arm_manager.sm_to_capping_station()
        self.solid_liquid_handling_manager.sm.move_needle(self.solid_liquid_handling_manager.SM_MOVE_DISPENSE)

        self.solid_liquid_handling_manager.sample_pump.pump_to_ml(
            volume * self.solid_liquid_handling_manager.SYRINGE_PUMP_CALIBRATION_FACTOR + 0.25, 3, 2)

        self.arm_manager.time_check_point = datetime.datetime.now()

        time.sleep(10)
        self.solid_liquid_handling_manager.sm.move_needle(-self.solid_liquid_handling_manager.SM_MOVE_DISPENSE)
        self.arm_manager.sm_from_integrity()
        self.arm_manager.sm_to_base()

        self.solid_liquid_handling_manager.sample_pump.home()

    def direct_inject(self, reactor_row: int, reactor_column: int, solvent: int = VAPORTECH_TO_THF,
                      with_methanol: bool = True, push_vol=0.55):
        self.solid_liquid_handling_manager.wash(5, solvent=solvent)
        self.solid_liquid_handling_manager.sample_valve.switch_valve(self.solid_liquid_handling_manager.SAMPLE_BYPASS)
        for i in range(3):
            self.solid_liquid_handling_manager. \
                sample_pump.dispense_ml(5, 3, 2, self.solid_liquid_handling_manager.PUMP_VELOCITY_FAST)
        # # # perfect timing example in methods.py
        # #
        self.arm_manager.sm_from_base()
        self.arm_manager.sm_to_integrity(reactor_row, reactor_column)
        self.solid_liquid_handling_manager.sm.move_needle(self.solid_liquid_handling_manager.SM_MOVE_TALL_WITHDRAW)
        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(0.15, 3, 1)
        time.sleep(10)
        self.solid_liquid_handling_manager.sm.move_needle(-self.solid_liquid_handling_manager.SM_MOVE_TALL_WITHDRAW)

        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(self.solid_liquid_handling_manager.TRAVEL_LENGTH, 3,
                                                                    0.5)
        time.sleep(0.1)
        self.solid_liquid_handling_manager.sample_valve.switch_valve(self.solid_liquid_handling_manager.SAMPLE_INLINE)
        time.sleep(0.1)
        self.solid_liquid_handling_manager. \
            sample_pump.move_relative_ml(self.solid_liquid_handling_manager.SECOND_AIRGAP_TRANSFER,
                                         self.solid_liquid_handling_manager.SAMPLE_DRAW_RATE)
        time.sleep(5)

        self.solid_liquid_handling_manager.sample_valve.switch_valve(self.solid_liquid_handling_manager.SAMPLE_BYPASS)
        self.solid_liquid_handling_manager.hplc_valve.switch_valve(self.solid_liquid_handling_manager.HPLC_INJECT)

        # draw by push pump:
        if with_methanol:
            self.solid_liquid_handling_manager.push_pump.switch_valve(self.solid_liquid_handling_manager.SOLVENT_PORT)
        else:
            self.solid_liquid_handling_manager.push_pump.switch_valve(self.solid_liquid_handling_manager.AIR_PORT)
        self.solid_liquid_handling_manager.push_pump.move_absolute_ml(self.solid_liquid_handling_manager.PUSH_PUMP_VOL,
                                                                      self.solid_liquid_handling_manager.SOLVENT_DRAW_RATE_PPUMP)
        time.sleep(4)

        # move sample and solvent to valve_b
        self.solid_liquid_handling_manager.push_pump.switch_valve(self.solid_liquid_handling_manager.SAMPLE_PORT)
        self.solid_liquid_handling_manager.push_pump.move_relative_ml(push_vol
                                                                      ,
                                                                      self.solid_liquid_handling_manager.SOLVENT_PUSH_RATE_PPUMP)
        time.sleep(3)
        self.solid_liquid_handling_manager.hplc_valve.switch_valve(self.solid_liquid_handling_manager.HPLC_FILL)
        self.solid_liquid_handling_manager.push_pump.move_relative_ml(-0.010
                                                                      ,
                                                                      self.solid_liquid_handling_manager.PUSH_RATE_FINAL)

        # trigger HPLC
        self.solid_liquid_handling_manager.relays.set_output(3, True)
        time.sleep(3)
        self.solid_liquid_handling_manager.relays.set_output(3, False)
        time.sleep(3)
        time.sleep(1)
        self.arm_manager.sm_from_integrity()
        time.sleep(1)
        self.arm_manager.sm_to_base()
        # phase C: switch sample loop on valve_b and trigger hplc
        self.solid_liquid_handling_manager.hplc_valve.switch_valve(self.solid_liquid_handling_manager.HPLC_INJECT)
        # empty both pumps
        self.solid_liquid_handling_manager.sample_pump.switch_valve(self.solid_liquid_handling_manager.SOLVENT_PORT)
        self.solid_liquid_handling_manager.sample_pump.move_absolute_ml(0,
                                                                        velocity_ml=self.solid_liquid_handling_manager.SAMPLE_FLUSH_RATE,
                                                                        wait=False)
        # self.solid_liquid_handling_manager.push_pump.switch_valve(self.solid_liquid_handling_manager.AIR_PORT)
        self.solid_liquid_handling_manager.push_pump.move_absolute_ml(0,
                                                                      velocity_ml=self.solid_liquid_handling_manager.SOLVENT_PUSH_RATE,
                                                                      wait=False)
        self.solid_liquid_handling_manager.TecanCavro.wait_for_all(self.solid_liquid_handling_manager.sample_pump,
                                                                   self.solid_liquid_handling_manager.push_pump)

    def take_hplc_sample_from_n9(self, row: int, column: int):

        raise ('Please revise the following before using this method.')
        self.solid_liquid_handling_manager.wash(5)
        self.solid_liquid_handling_manager.hplc_valve.switch_valve('B')
        self.solid_liquid_handling_manager.sample_valve.switch_valve('B')
        self.solid_liquid_handling_manager.sample_pump.dispense_ml(15, 3, 2,
                                                                   self.solid_liquid_handling_manager.PUMP_VELOCITY_FAST)
        self.arm_manager.vial_from_n9()
        self.arm_manager.vial_to_kinova()
        self.arm_manager.sm_from_base()
        self.arm_manager.sm_to_hplc_tray()
        self.solid_liquid_handling_manager.sm.move_needle(self.solid_liquid_handling_manager.SM_MOVE_HPLC_WITHDRAW)
        time.sleep(5)
        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(0.15, 3, 2)
        self.solid_liquid_handling_manager.sm.home()
        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(self.solid_liquid_handling_manager.TRAVEL_LENGTH, 3,
                                                                    1.0)
        self.solid_liquid_handling_manager.sample_valve.switch_valve(self.solid_liquid_handling_manager.SAMPLE_INLINE)
        self.solid_liquid_handling_manager.sample_pump.move_relative_ml(
            self.solid_liquid_handling_manager.SECOND_AIRGAP_TRANSFER,
            self.solid_liquid_handling_manager.SAMPLE_DRAW_RATE)
        time.sleep(5)
        self.solid_liquid_handling_manager.sample_valve.switch_valve(self.solid_liquid_handling_manager.SAMPLE_BYPASS)
        self.solid_liquid_handling_manager.sm.move_needle(-self.solid_liquid_handling_manager.SM_MOVE_HPLC_WITHDRAW)
        self.arm_manager.sm_from_hplc_tray()
        self.arm_manager.sm_to_base()
        self.relocate_hplc_vial(row, column)

    def take_hplc_sample_from_tray(self):
        ##
        raise ('Please revise the following before using this method.')
        self.solid_liquid_handling_manager.wash_by_syringe_pump(5)
        self.solid_liquid_handling_manager.hplc_valve.switch_valve('B')
        self.solid_liquid_handling_manager.sample_valve.switch_valve('B')
        self.solid_liquid_handling_manager.sample_pump.dispense_ml(0.5, 3, 2,
                                                                   self.solid_liquid_handling_manager.PUMP_VELOCITY_SLOW)
        self.arm_manager.sm_from_base()
        self.arm_manager.sm_to_hplc_tray()
        self.solid_liquid_handling_manager.sm.move_needle(self.solid_liquid_handling_manager.SM_MOVE_HPLC_WITHDRAW)
        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(0.1, 3, 2)
        self.solid_liquid_handling_manager.sm.home()
        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(self.solid_liquid_handling_manager.TRAVEL_LENGTH, 3,
                                                                    1.0)

        self.arm_manager.sm_from_hplc_tray()
        self.arm_manager.sm_to_base()

        self.solid_liquid_handling_manager.hplc_valve.switch_valve(self.solid_liquid_handling_manager.HPLC_FILL)
        time.sleep(3)
        self.solid_liquid_handling_manager.sample_pump.move_relative_ml(0.05,
                                                                        self.solid_liquid_handling_manager.PUSH_RATE_FINAL)
        time.sleep(4)

        self.solid_liquid_handling_manager.hplc_valve.switch_valve(self.solid_liquid_handling_manager.HPLC_INJECT)
        self.solid_liquid_handling_manager.relays.set_output(3, True)
        time.sleep(3)
        self.solid_liquid_handling_manager.relays.set_output(3, False)
        self.solid_liquid_handling_manager.sample_pump.switch_valve(self.solid_liquid_handling_manager.SOLVENT_PORT)
        self.solid_liquid_handling_manager.sample_pump.move_absolute_ml(0,
                                                                        velocity_ml=self.solid_liquid_handling_manager.SAMPLE_FLUSH_RATE,
                                                                        wait=True)
        self.solid_liquid_handling_manager.push_pump.move_absolute_ml(0,
                                                                      velocity_ml=self.solid_liquid_handling_manager.SOLVENT_PUSH_RATE,
                                                                      wait=True)

        self.solid_liquid_handling_manager.wash_by_syringe_pump(5)
        self.solid_liquid_handling_manager.TecanCavro.wait_for_all(self.solid_liquid_handling_manager.sample_pump,
                                                                   self.solid_liquid_handling_manager.push_pump)

    def take_hplc_sample(self, row=1, column=1, vial_holder='integrity', time_interval = None, sample_volume = 0.1,
                         derivatization_setup: bool= False):
        if time_interval is not None:
            wait = time_interval - (datetime.datetime.now() - self.hplc_injection_time).seconds
            if wait >= 0:
                print('Wait for', wait, 'seconds')
                if wait > 30:
                    self.arm_manager.arm.home(wait=False)
                time.sleep(wait)
        threading.Thread(target=turn_on_pump, args=[0]).start()
        self.hplc_injection_time = datetime.datetime.now()
        self.solid_liquid_handling_manager.hplc_valve.switch_valve('B')
        self.solid_liquid_handling_manager.sample_valve.switch_valve('B')
        self.solid_liquid_handling_manager.push_pump.dispense_ml(2,2,3,2,wait=False)
        self.solid_liquid_handling_manager.wash_by_syringe_pump(5)

        self.solid_liquid_handling_manager.sample_pump.dispense_ml(0.1, 3, 2,
                                                                   self.solid_liquid_handling_manager.PUMP_VELOCITY_SLOW)
        self.arm_manager.sm_from_base()
        if vial_holder == 'integrity':
            self.arm_manager.sm_to_integrity(row, column)
        elif vial_holder == 'ika':
            self.arm_manager.sm_to_ika(row, column)
        self.solid_liquid_handling_manager.sm.move_needle(self.solid_liquid_handling_manager.SM_MOVE_TALL_WITHDRAW)
        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(
            sample_volume * self.solid_liquid_handling_manager.SYRINGE_PUMP_CALIBRATION_FACTOR
            , 3, 2)
        time.sleep(5)
        self.solid_liquid_handling_manager.sm.move_needle(-self.solid_liquid_handling_manager.SM_MOVE_TALL_WITHDRAW)
        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(self.solid_liquid_handling_manager.TRAVEL_LENGTH, 3,
                                                                    1.00)

        self.arm_manager.sm_return_safe_pose_v()
        self.arm_manager.sm_to_base()
        if derivatization_setup:
            self.solid_liquid_handling_manager.sample_valve.switch_valve(
                self.solid_liquid_handling_manager.SAMPLE_INLINE)
            time.sleep(3)
            self.solid_liquid_handling_manager.sample_pump.move_relative_ml(0.03,
                                                                            self.solid_liquid_handling_manager.PUSH_RATE_FINAL)
            self.solid_liquid_handling_manager.sample_valve.switch_valve(self.solid_liquid_handling_manager.SAMPLE_BYPASS)
            self.solid_liquid_handling_manager.hplc_valve.switch_valve(self.solid_liquid_handling_manager.HPLC_INJECT)
            self.solid_liquid_handling_manager.push_pump.switch_valve(self.solid_liquid_handling_manager.SOLVENT_PORT)
            self.solid_liquid_handling_manager.push_pump.move_absolute_ml(self.solid_liquid_handling_manager.PUSH_PUMP_VOL,
                                                                          self.solid_liquid_handling_manager.SOLVENT_DRAW_RATE_PPUMP)
            time.sleep(4)

            # move sample and solvent to valve_b
            self.solid_liquid_handling_manager.push_pump.switch_valve(self.solid_liquid_handling_manager.SAMPLE_PORT)
            self.solid_liquid_handling_manager.push_pump.move_relative_ml(self.solid_liquid_handling_manager.SECOND_TRAVEL_LENGTH
                                                                          , self.solid_liquid_handling_manager.SOLVENT_PUSH_RATE_PPUMP)
            time.sleep(3)
            self.solid_liquid_handling_manager.hplc_valve.switch_valve(self.solid_liquid_handling_manager.HPLC_FILL)
            self.solid_liquid_handling_manager.push_pump.move_relative_ml(-0.05
                                            , self.solid_liquid_handling_manager.PUSH_RATE_FINAL)
        else:
            self.solid_liquid_handling_manager.hplc_valve.switch_valve(self.solid_liquid_handling_manager.HPLC_FILL)
            time.sleep(3)
            self.solid_liquid_handling_manager.sample_pump.move_relative_ml(0.03,
                                                                            self.solid_liquid_handling_manager.PUSH_RATE_FINAL)
        time.sleep(10)

        self.solid_liquid_handling_manager.relays.set_output(3, True)
        time.sleep(3)
        self.solid_liquid_handling_manager.relays.set_output(3, False)
        time.sleep(3)
        self.solid_liquid_handling_manager.hplc_valve.switch_valve(self.solid_liquid_handling_manager.HPLC_INJECT)
        self.solid_liquid_handling_manager.sample_pump.switch_valve(self.solid_liquid_handling_manager.SOLVENT_PORT)
        self.solid_liquid_handling_manager.sample_pump.move_absolute_ml(0,
                                                                        velocity_ml=self.solid_liquid_handling_manager.SAMPLE_FLUSH_RATE,
                                                                        wait=True)
        self.solid_liquid_handling_manager.push_pump.move_absolute_ml(0,
                                                                      velocity_ml=self.solid_liquid_handling_manager.SOLVENT_PUSH_RATE,
                                                                      wait=True)
        self.arm_manager.arm.home()
        # self.solid_liquid_handling_manager.wash_by_syringe_pump(5)
        self.solid_liquid_handling_manager.TecanCavro.wait_for_all(self.solid_liquid_handling_manager.sample_pump,
                                                                   self.solid_liquid_handling_manager.push_pump)

    def dose_mg(self, mass: float):
        # Assume vial inside quantos
        # mass>=70
        self.solid_liquid_handling_manager.quantos.home_z_stage()
        self.solid_liquid_handling_manager.quantos.move_z_stage(
            self.solid_liquid_handling_manager.QUANTOS_STEP_INTEGRITY_VIAL)
        self.solid_liquid_handling_manager.quantos.target_mass = mass  # mg
        self.solid_liquid_handling_manager.quantos.start_dosing(wait_for=True)

        x = self.solid_liquid_handling_manager.quantos.sample_data.quantity
        x = float(x)
        print(mass - x)
        # loop to fix dosing error
        while mass - x > 5:
            self.solid_liquid_handling_manager.quantos.target_mass = float(mass - x)  # mg
            self.solid_liquid_handling_manager.quantos.start_dosing(wait_for=True)
            y = self.solid_liquid_handling_manager.quantos.sample_data.quantity
            x += float(y)
        mass_no_units = float(x)  # mg
        print('dosed ', mass_no_units, 'mg')
        # z stage up
        self.solid_liquid_handling_manager.quantos.home_z_stage()
        return mass_no_units

    def vial_to_quantos(self):
        safe_pose_h_side = self.arm_manager.action_sequences['regripping_action']['safe_pose_h_side']
        quan_out = self.arm_manager.action_sequences['quantos_']['quan_out']
        quan_in_lift = self.arm_manager.action_sequences['quantos_']['quan_in_lift']
        quan_in_grab = self.arm_manager.action_sequences['quantos_']['quan_in_grab']
        self.solid_liquid_handling_manager.quantos.home_z_stage()

        self.arm_manager.arm.move_to_locations(safe_pose_h_side)
        self.solid_liquid_handling_manager.quantos.front_door_position = 'open'
        self.arm_manager.arm.move_to_locations(quan_out)
        self.arm_manager.arm.move_to_locations(quan_in_lift.translate(y=30))
        self.arm_manager.arm.move_to_locations(quan_in_lift)
        self.arm_manager.arm.move_to_locations(quan_in_grab)
        self.arm_manager.arm.open_gripper(self.arm_manager.GRIPPER_OPEN_DEFAULT)
        self.arm_manager.arm.move_to_locations(quan_in_lift)
        self.arm_manager.arm.move_to_locations(quan_in_lift.translate(y=50))
        self.arm_manager.arm.move_to_locations(quan_out)

        self.arm_manager.arm.move_to_locations(safe_pose_h_side)
        try:
            self.solid_liquid_handling_manager.quantos.front_door_position = 'close'
        except ValueError:
            pass

    def vial_from_quantos(self):
        safe_pose_h_side = self.arm_manager.action_sequences['regripping_action']['safe_pose_h_side']
        quan_out = self.arm_manager.action_sequences['quantos_']['quan_out']
        quan_in_lift = self.arm_manager.action_sequences['quantos_']['quan_in_lift']
        quan_in_grab = self.arm_manager.action_sequences['quantos_']['quan_in_grab']
        self.solid_liquid_handling_manager.quantos.home_z_stage()
        self.arm_manager.arm.move_to_locations(safe_pose_h_side)
        self.solid_liquid_handling_manager.quantos.front_door_position = 'open'
        self.arm_manager.arm.move_to_locations(quan_out)
        self.arm_manager.arm.open_gripper(self.arm_manager.GRIPPER_OPEN_DEFAULT)
        self.arm_manager.arm.move_to_locations(quan_in_lift.translate(y=50))
        self.arm_manager.arm.move_to_locations(quan_in_lift)
        self.arm_manager.arm.move_to_locations(quan_in_grab)
        self.arm_manager.arm.open_gripper(self.arm_manager.GRIPPER_CLOSE_DEFAULT)
        time.sleep(2)
        self.arm_manager.arm.move_to_locations(quan_in_lift)
        self.arm_manager.arm.move_to_locations(quan_in_lift.translate(y=50))
        self.arm_manager.arm.move_to_locations(quan_out)
        self.arm_manager.arm.move_to_locations(safe_pose_h_side)
        try:
            self.solid_liquid_handling_manager.quantos.front_door_position = 'close'
        except ValueError:
            pass

    def get_mass_from_dosed_volume(self, volume):
        # self.solid_liquid_handling_manager.sm.move_needle(self.solid_liquid_handling_manager.SM_MOVE_DISPENSE)
        # self.solid_liquid_handling_manager.pump_by_vaportech(2)
        # self.solid_liquid_handling_manager.sm.move_needle(-self.solid_liquid_handling_manager.SM_MOVE_DISPENSE)
        self.arm_manager.vial_from_tray(3, 2)
        self.arm_manager.vial_to_regripping_v()
        self.arm_manager.vial_from_regripping_h()
        self.vial_to_quantos()
        before_mass = self.solid_liquid_handling_manager.quantos.weight_immediately
        print(before_mass)
        self.vial_from_quantos()
        self.arm_manager.vial_to_regripping_h()
        self.arm_manager.vial_from_regripping_v()
        self.arm_manager.vial_to_integrity(5, 2)
        self.arm_manager.sm_from_base()
        self.arm_manager.sm_to_integrity(5, 2)
        self.solid_liquid_handling_manager.sm.move_needle(self.solid_liquid_handling_manager.SM_MOVE_DISPENSE)
        self.solid_liquid_handling_manager.pump_by_vaportech(volume)
        self.solid_liquid_handling_manager.sm.move_needle(-self.solid_liquid_handling_manager.SM_MOVE_DISPENSE)
        self.arm_manager.sm_from_integrity()
        self.arm_manager.sm_to_base()
        self.arm_manager.vial_from_integrity(5, 2)
        self.arm_manager.vial_to_regripping_v()
        self.arm_manager.vial_from_regripping_h()
        self.vial_to_quantos()
        after_mass = self.solid_liquid_handling_manager.quantos.weight_immediately
        print(after_mass)
        self.vial_from_quantos()
        self.arm_manager.vial_to_regripping_h()
        self.arm_manager.vial_from_regripping_v()
        self.arm_manager.vial_to_tray(3, 2)
        return float(after_mass) - float(before_mass)

    def air_purge(self):
        self.solid_liquid_handling_manager.wash(2.5)
        self.solid_liquid_handling_manager.sample_pump.dispense_ml(5, 3, 2,
                                                                   self.solid_liquid_handling_manager.PUMP_VELOCITY_FAST)
        self.solid_liquid_handling_manager.sample_pump.dispense_ml(5, 3, 2,
                                                                   self.solid_liquid_handling_manager.PUMP_VELOCITY_FAST)
        self.solid_liquid_handling_manager.sample_pump.dispense_ml(5, 3, 2,
                                                                   self.solid_liquid_handling_manager.PUMP_VELOCITY_FAST)
        self.arm_manager.sm_from_base()
        self.arm_manager.sm_to_integrity(1, 1)
        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(5, 3, 20)
        self.solid_liquid_handling_manager.sm.move_needle(self.solid_liquid_handling_manager.SM_MOVE_WITHDRAW)
        self.solid_liquid_handling_manager.sample_pump.pump_to_ml(4, 3, 20)
        self.solid_liquid_handling_manager.sm.move_needle(-self.solid_liquid_handling_manager.SM_MOVE_WITHDRAW)

        for i in range(24):
            self.solid_liquid_handling_manager.sample_pump.pump_from_ml(4, 3, 20)
            self.solid_liquid_handling_manager.sm.move_needle(68)
            self.solid_liquid_handling_manager.sample_pump.pump_to_ml(4, 3, 20)
            self.solid_liquid_handling_manager.sm.move_needle(68)

        self.solid_liquid_handling_manager.sm.home()
        self.arm_manager.sm_from_integrity()
        self.arm_manager.sm_to_base()

    def nitrogen_purge(self, reactor_row: int = 1, reactor_column: int = 1,
                       ir = None,
                       vial_holder = 'integrity',
                       time_out: float = 300):
        self.solid_liquid_handling_manager.wash(2.5)
        self.solid_liquid_handling_manager.relays.set_output(2, True)
        time.sleep(10)

        self.solid_liquid_handling_manager.relays.set_output(2, False)
        self.arm_manager.sm_from_base()
        if vial_holder == 'integrity':
            self.arm_manager.sm_to_integrity(reactor_row, reactor_column)
        elif vial_holder == 'ika':
            self.arm_manager.sm_to_ika(reactor_row,reactor_column)
        else:
            raise ValueError

        self.solid_liquid_handling_manager.sm.move_needle(self.solid_liquid_handling_manager.SM_MOVE_TALL_WITHDRAW)
        self.solid_liquid_handling_manager.relays.set_output(2, True)
        now = datetime.datetime.now()
        if ir is None:
            time.sleep(time_out)

        else:
            time.sleep(120)
            i = 0
            while not ir.is_plateau('CO2',amount_of_interest=2) and not i < time_out/60:
                time.sleep(60)
                i += 1
        after = datetime.datetime.now()
        duration = abs((after - now).seconds)
        lucky.post_slack_message('Purging cost '+ str(duration)+' seconds.')
        self.solid_liquid_handling_manager.sm.move_needle(-self.solid_liquid_handling_manager.SM_MOVE_TALL_WITHDRAW)
        self.solid_liquid_handling_manager.relays.set_output(2, False)
        self.arm_manager.sm_to_base()

    def relocate_hplc_vial(self,
                           end_row: int,
                           end_column: int,
                           start_row: int = 1,
                           start_column: int = 1):
        start_position = self.arm_manager.grid_location(start_row, start_column, 'N_v')
        end_location = self.arm_manager.grid_location(end_row, end_column, 'N_v')
        safe_pose_h_side = self.arm_manager.action_sequences['regripping_action']['safe_pose_v']
        start_above = start_position.translate(z=100)
        end_above = end_location.translate(z=100)
        self.arm_manager.arm.move_to_locations(safe_pose_h_side, start_above)
        self.arm_manager.arm.open_gripper(self.arm_manager.GRIPPER_OPEN_HPLC_VIAL)
        self.arm_manager.arm.move_to_locations(start_position)
        self.arm_manager.arm.open_gripper(self.arm_manager.GRIPPER_CLOSE_HPLC_VIAL)
        time.sleep(2)
        self.arm_manager.arm.move_to_locations(start_above, end_above, end_location)
        self.arm_manager.arm.open_gripper(self.arm_manager.GRIPPER_OPEN_HPLC_VIAL)
        self.arm_manager.arm.move_to_locations(end_above, safe_pose_h_side)

    def dispense_solvent_to_capping(self, solvent: int = VAPORTECH_TO_THF, volume: float = 3):
        if not solvent == self.solid_liquid_handling_manager.VAPOURTEC_TO_FILTER_BEAKER:
            self.solid_liquid_handling_manager.wash(5, solvent=solvent)
        else:
            self.solid_liquid_handling_manager.wash(5, solvent=self.solid_liquid_handling_manager.VAPOURTEC_TO_AIR)
        self.arm_manager.sm_from_base()
        self.arm_manager.sm_to_capping_station()
        time.sleep(1)
        self.solid_liquid_handling_manager.sm.home()
        self.solid_liquid_handling_manager.sm.move_needle(self.solid_liquid_handling_manager.SM_MOVE_DISPENSE)
        self.solid_liquid_handling_manager.vici10.switch_valve(solvent)
        self.solid_liquid_handling_manager.pump_by_vaportech(volume)
        time.sleep(1)
        self.solid_liquid_handling_manager.sm.move_needle(-self.solid_liquid_handling_manager.SM_MOVE_DISPENSE)
        self.solid_liquid_handling_manager.sm.home()
        self.arm_manager.sm_from_capping_station()
        self.arm_manager.sm_to_base()
        self.solid_liquid_handling_manager.vici10.switch_valve(self.solid_liquid_handling_manager.VAPOURTEC_TO_AIR)
        self.solid_liquid_handling_manager.pump_by_vaportech(10)  ## Empty the line

    def liquid_transfer_from_capping_to_centrifuge(self, centrifuge_row: int, centrifuge_column: int,
                                                   volume: float = 0.8,
                                                   tilt_left: bool = True):
        """
        Tranfer small amount liquid from capping station to a centrifuge tube
        """

        self.arm_manager.sm_from_base()
        self.arm_manager.sm_to_capping_station(tilt_left=tilt_left)
        self.solid_liquid_handling_manager.sample_pump.dispense_ml(
            0.25, 3, 2,
            velocity_ml=self.solid_liquid_handling_manager.PUMP_VELOCITY_DEFAULT)
        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(
            0.25, 3,
            velocity_ml=self.solid_liquid_handling_manager.PUMP_VELOCITY_SLOW)
        self.solid_liquid_handling_manager.sm.move_needle(self.solid_liquid_handling_manager.SM_MOVE_TALL_WITHDRAW)
        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(
            volume * self.solid_liquid_handling_manager.SYRINGE_PUMP_CALIBRATION_FACTOR, 3,
            velocity_ml=self.solid_liquid_handling_manager.PUMP_VELOCITY_SLOW)

        time.sleep(10)
        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(
            0.1, 3,
            velocity_ml=self.solid_liquid_handling_manager.PUMP_VELOCITY_SLOW)
        self.solid_liquid_handling_manager.sm.move_needle(-self.solid_liquid_handling_manager.SM_MOVE_TALL_WITHDRAW)
        self.arm_manager.sm_return_safe_pose_v()
        self.arm_manager.sm_to_centrifuge_tray(centrifuge_row, centrifuge_column)
        self.solid_liquid_handling_manager.sm.move_needle(
            self.solid_liquid_handling_manager.SM_MOVE_CENTRIFUGE_DISPENSE)
        self.solid_liquid_handling_manager.sample_pump.switch_valve(self.solid_liquid_handling_manager.SAMPLE_PORT)
        self.solid_liquid_handling_manager.sample_pump.move_absolute_ml(0,
                                                                        velocity_ml=self.solid_liquid_handling_manager.PUMP_VELOCITY_SLOW)
        self.solid_liquid_handling_manager.sm.move_needle(
            -self.solid_liquid_handling_manager.SM_MOVE_CENTRIFUGE_DISPENSE)

        self.arm_manager.sm_return_safe_pose_v()

        self.arm_manager.sm_to_base()
        self.solid_liquid_handling_manager.sample_pump.home()

    def transfer_all_from_capping_to_one_centrifuge(self, centrifuge_row: int, centrifuge_column: int):
        self.solid_liquid_handling_manager.bypass_setup()
        self.solid_liquid_handling_manager.wash_by_syringe_pump(5)

        self.arm_manager.sm_from_base()
        self.liquid_transfer_from_capping_to_centrifuge(centrifuge_row=centrifuge_row,
                                                        centrifuge_column=centrifuge_column,
                                                        volume=1.5,
                                                        tilt_left=True)
        self.liquid_transfer_from_capping_to_centrifuge(centrifuge_row=centrifuge_row,
                                                        centrifuge_column=centrifuge_column,
                                                        volume=1.5,
                                                        tilt_left=False)
        self.arm_manager.sm_to_base()

    def transfer_all_from_capping_to_all_three_centrifuges(self, handle_row: Optional = None,
                                                           handle_column: Optional = None):
        self.solid_liquid_handling_manager.bypass_setup()
        self.solid_liquid_handling_manager.wash_by_syringe_pump(5)
        if handle_row is not None and handle_column is not None:
            for i in range(2):
                for j in range(2):
                    if j is not handle_column - 1 or i is not handle_row - 1:
                        self.liquid_transfer_from_capping_to_centrifuge(centrifuge_row=i + 1,
                                                                        centrifuge_column=j + 1,
                                                                        volume=0.5,
                                                                        tilt_left=True)
        else:
            for i in range(2):
                for j in range(2):
                    self.liquid_transfer_from_capping_to_centrifuge(centrifuge_row=i + 1,
                                                                    centrifuge_column=j + 1,
                                                                    volume=0.5,
                                                                    tilt_left=True)
        self.arm_manager.sm_to_base()

    def liquid_transfer_from_centrifuge_to_capping(self, centrifuge_row: int, centrifuge_column: int,
                                                   volume: float = 0.8):
        """
        Transfer liquid from centrifuge to capping station's vial
        ASSUME volume <= 2
        """

        # AIR--LIQUID--air Method
        self.solid_liquid_handling_manager.sample_pump.dispense_ml(0.25, 3, 2,
                                                                   velocity_ml=self.solid_liquid_handling_manager.PUMP_VELOCITY_DEFAULT)
        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(0.25, 3,
                                                                    velocity_ml=self.solid_liquid_handling_manager.PUMP_VELOCITY_SLOW)

        self.arm_manager.sm_to_centrifuge_tray(centrifuge_row, centrifuge_column)
        self.solid_liquid_handling_manager.sm.move_needle(
            self.solid_liquid_handling_manager.SM_MOVE_CENTRIFUGE_WITHDRAW)
        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(
            volume * self.solid_liquid_handling_manager.SYRINGE_PUMP_CALIBRATION_FACTOR,
            3,
            velocity_ml=self.solid_liquid_handling_manager.PUMP_VELOCITY_SLOW)

        time.sleep(10)

        self.solid_liquid_handling_manager.sm.move_needle(
            -self.solid_liquid_handling_manager.SM_MOVE_CENTRIFUGE_WITHDRAW)
        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(0.1, 3,
                                                                    velocity_ml=self.solid_liquid_handling_manager.PUMP_VELOCITY_SLOW)
        self.arm_manager.sm_to_capping_station()
        self.solid_liquid_handling_manager.sm.move_needle(self.solid_liquid_handling_manager.SM_MOVE_DISPENSE)
        self.solid_liquid_handling_manager.sample_pump.move_absolute_ml(0,
                                                                        velocity_ml=self.solid_liquid_handling_manager.PUMP_VELOCITY_SLOW)
        time.sleep(10)
        self.solid_liquid_handling_manager.sm.move_needle(-self.solid_liquid_handling_manager.SM_MOVE_DISPENSE)

        self.arm_manager.sm_return_safe_pose_v()

    def transfer_all_from_three_centrifuge_tubes_to_capping(self, handle_row: int = 1, handle_column: int = 2):
        """
        Transfer from all three centrifuge ports (except for the handle) to the capping station, with vial uncapped2
        """
        self.solid_liquid_handling_manager.wash_by_syringe_pump(5)
        self.solid_liquid_handling_manager.bypass_setup()
        self.arm_manager.sm_from_base()
        for i in range(2):
            for j in range(2):
                if j is not handle_column - 1 or i is not handle_row - 1:
                    self.liquid_transfer_from_centrifuge_to_capping(i + 1, j + 1,
                                                                    volume=1 * self.solid_liquid_handling_manager.SYRINGE_PUMP_CALIBRATION_FACTOR)

        self.arm_manager.sm_to_base()
        self.solid_liquid_handling_manager.sample_pump.home()

    def direct_inject_centrifuge(self, exp_solvent: int, centrifuge_row: int = 1, centrifuge_column: int = 1, ):
        self.solid_liquid_handling_manager.wash(5, solvent=exp_solvent)
        self.solid_liquid_handling_manager.sample_pump.dispense_ml(15, 3, 2,
                                                                   self.solid_liquid_handling_manager.PUMP_VELOCITY_FAST)
        self.solid_liquid_handling_manager.sample_valve.switch_valve(self.solid_liquid_handling_manager.SAMPLE_BYPASS)
        self.arm_manager.sm_from_base()
        self.arm_manager.sm_to_centrifuge_tray(centrifuge_row, centrifuge_column)
        self.solid_liquid_handling_manager.sm.move_needle(
            self.solid_liquid_handling_manager.SM_MOVE_CENTRIFUGE_WITHDRAW)
        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(0.15, 3, 1)
        time.sleep(10)
        self.solid_liquid_handling_manager.sm.move_needle(
            -self.solid_liquid_handling_manager.SM_MOVE_CENTRIFUGE_WITHDRAW)

        self.solid_liquid_handling_manager.sample_pump.pump_from_ml(self.solid_liquid_handling_manager.TRAVEL_LENGTH, 3,
                                                                    0.5)
        time.sleep(0.1)
        self.solid_liquid_handling_manager.sample_valve.switch_valve(self.solid_liquid_handling_manager.SAMPLE_INLINE)
        time.sleep(0.1)
        self.solid_liquid_handling_manager. \
            sample_pump.move_relative_ml(self.solid_liquid_handling_manager.SECOND_AIRGAP_TRANSFER,
                                         self.solid_liquid_handling_manager.SAMPLE_DRAW_RATE)
        time.sleep(5)

        self.solid_liquid_handling_manager.sample_valve.switch_valve(self.solid_liquid_handling_manager.SAMPLE_BYPASS)
        self.solid_liquid_handling_manager.hplc_valve.switch_valve(self.solid_liquid_handling_manager.HPLC_INJECT)

        # draw by push pump:
        self.solid_liquid_handling_manager.push_pump.switch_valve(self.solid_liquid_handling_manager.AIR_PORT)
        self.solid_liquid_handling_manager.push_pump.move_absolute_ml(self.solid_liquid_handling_manager.PUSH_PUMP_VOL,
                                                                      self.solid_liquid_handling_manager.SOLVENT_DRAW_RATE_PPUMP)
        time.sleep(4)

        # move sample and solvent to valve_b
        self.solid_liquid_handling_manager.push_pump.switch_valve(self.solid_liquid_handling_manager.SAMPLE_PORT)
        self.solid_liquid_handling_manager.push_pump.move_relative_ml(0.55
                                                                      ,
                                                                      self.solid_liquid_handling_manager.SOLVENT_PUSH_RATE_PPUMP)
        time.sleep(3)
        self.solid_liquid_handling_manager.hplc_valve.switch_valve(self.solid_liquid_handling_manager.HPLC_FILL)
        self.solid_liquid_handling_manager.push_pump.move_relative_ml(-0.010
                                                                      ,
                                                                      self.solid_liquid_handling_manager.PUSH_RATE_FINAL)

        # trigger HPLC
        self.solid_liquid_handling_manager.relays.set_output(3, True)
        time.sleep(3)
        self.solid_liquid_handling_manager.relays.set_output(3, False)
        time.sleep(3)
        time.sleep(1)
        self.arm_manager.sm_return_safe_pose_v()
        time.sleep(1)
        self.arm_manager.sm_to_base()
        # phase C: switch sample loop on valve_b and trigger hplc
        self.solid_liquid_handling_manager.hplc_valve.switch_valve(self.solid_liquid_handling_manager.HPLC_INJECT)
        # empty both pumps
        self.solid_liquid_handling_manager.sample_pump.switch_valve(self.solid_liquid_handling_manager.SOLVENT_PORT)
        self.solid_liquid_handling_manager.sample_pump.move_absolute_ml(0,
                                                                        velocity_ml=self.solid_liquid_handling_manager.SAMPLE_FLUSH_RATE,
                                                                        wait=False)
        # self.solid_liquid_handling_manager.push_pump.switch_valve(self.solid_liquid_handling_manager.AIR_PORT)
        self.solid_liquid_handling_manager.push_pump.move_absolute_ml(0,
                                                                      velocity_ml=self.solid_liquid_handling_manager.SOLVENT_PUSH_RATE,
                                                                      wait=False)
        self.solid_liquid_handling_manager.TecanCavro.wait_for_all(self.solid_liquid_handling_manager.sample_pump,
                                                                   self.solid_liquid_handling_manager.push_pump)

    def add_solid_using_funnel(self, mass: float,
                               integrity_row: int, integrity_column: int, funnel_row: int, funnel_column: int):

        # assuming a vial in Quantos, vial capped, and capping station is available

        ## returns the actual dosed mass
        dosed_mg = self.dose_mg(mass)
        self.arm_manager.uncap_integrity_vial(integrity_row, integrity_column)
        self.arm_manager.move_funnel(funnel_row, funnel_column,
                                     integrity_row, integrity_column)
        self.vial_from_quantos()
        self.arm_manager.pour_solid_into_integrity(integrity_row, integrity_column)
        self.vial_to_quantos()
        time.sleep(10)
        solid_left = float(self.solid_liquid_handling_manager.quantos.weight_immediately)
        self.arm_manager.move_funnel(integrity_row, integrity_column, funnel_row, funnel_column)
        self.arm_manager.cap_integrity_vial(integrity_row, integrity_column)

        return dosed_mg - solid_left


