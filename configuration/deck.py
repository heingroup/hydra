import datetime
import os

from ftdi_serial import Serial
from ika import MagneticStirrer
from north_devices.pumps.tecan_cavro import TecanCavro
from mtbalance import ArduinoAugmentedQuantos
from experiments.vicivalve import VICI
from sampleomatic_stepper.sampleomatic_stepper_api import SampleomaticStepper
from hein_robots.kinova.kinova_gen3 import KinovaGen3Arm
from hein_robots.kinova.kinova_sequence import KinovaSequence
from vapourtec.vapourtec.sf10 import SF10
from integrity.integrity.api import Integrity10
from .sequences.sequences import *
import time


# Kinova

arm = KinovaGen3Arm(default_velocity=100)
# Robotic locations
print('arm connected')
peak_times_and_labels = { }


reaction_folder = r'D:\Chemstation\1\Data\2021-12-17\hydra_kinova 2021-12-21 12-28-13'

ir_folder = r'D:\iClC & iCIR data files\iC IR Experiments\Hydra 074 Exp 2021-12-21 12-31'


time_check_point = datetime.datetime.now()  ### datetime of the reagent dose

sample_valve_serial = Serial(device_serial='FT0VORU5', baudrate=9600)  # 6 port
sample_valve = VICI(sample_valve_serial)
# sample_valve = 'stub'
hplc_valve_serial = Serial(device_serial="DO01YTO4", baudrate=9600)  # 6 port
hplc_valve = VICI(hplc_valve_serial)

# pumps
serial = Serial(device_serial='FT2YOS81', baudrate=9600)  # Cavro pumps default to 9600
sample_pump = TecanCavro(serial, address=1, syringe_volume_ml=5)

push_pump = TecanCavro(serial, address=2, syringe_volume_ml=2.5)

sample_pump.home()
sample_valve.switch_valve('A')
push_pump.switch_valve(3)
push_pump.move_absolute_ml(0,velocity_ml=5)
sample_pump.switch_valve(1)
sample_pump.home()
sample_pump.move_absolute_ml(0,velocity_ml=1)
hplc_valve.switch_valve('B')
sample_valve.switch_valve('B')

print('pumps connected')

# sampleomatic
sm = SampleomaticStepper(serial_port=13)
sm.home()
print('samplematic connected')

# quantos

quantos = ArduinoAugmentedQuantos('192.168.253.2', 6, tcp_port=8014)
quantos.set_home_direction(0)
quantos.home_z_stage()
print('Quantos connected')
# 10-port solvent selection valve
selection_valve_serial = Serial(device_port='COM12', baudrate=9600)
vici10 = VICI(selection_valve_serial, positions=10)

push_pump.home()

sf10 = SF10('COM3')
# push_pump.dispense_ml(5,1,3,5)
print('VICI connected')
# integrity = Integrity10(com_port=26)
# print('integrity connected')
ika = MagneticStirrer('COM17')
print('ika connected')
# relay board

from ftdi_serial import Serial
from experiments.relayboard import RelayBoard

relays = RelayBoard(Serial(device_serial='AG0JGDM1'))
print('relay connected')


def relay_1_On():
    relays.set_output(1, True)  # Toggle Relay 1 On


def relay_1_Off():
    relays.set_output(1, False)  # Toggle Relay 1 Off


def relay_2_On():
    relays.set_output(2, True)  # Toggle Relay 2 On


def relay_2_Off():
    relays.set_output(2, False)  # Toggle Relay 2 Off