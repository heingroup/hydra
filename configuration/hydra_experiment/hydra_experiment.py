import sys
import threading

import time
import traceback

from configuration.config import kinova_deck_manager
from configuration.kinova_deck_manager.kinova_deck_manager import KinovaDeckManager
from configuration.bot import bot
from experiments.utilities.hplc_pump_control.hplc_pump_control import turn_off_pump
from experiments.utilities.ir_analysis_tools.ir_analysis_tools import *
from experiments.utilities.hplc_analysis_tools.methods.peak_process_and_visualization import *


class HydraExperiment:

    def __init__(self,
                 kinova_deck_manager: KinovaDeckManager,
                 ):
        self.SAMPLE_VOLUME = 0.1
        self.HPLC_TIME_INTERVAL = 600  # s
        self.INTERNAL_STANDARD_RT = 2.7 # min, None if unknown

        self.hplc_folder = r''
        self.ir_folder = r''
        self.peak_times_and_labels = {}
        self.ir = None
        self.current_volume = 0
        self.kinova_deck_manager = kinova_deck_manager

    def run(self,
            acid_row: int = 1,
            acid_column: int = 3,
            amine_row: int = 1,
            amine_column: int = 1,
            is_row: int = 1,
            is_column: int = 2,
            water_row: int = 1,
            water_column: int = 4,
            vial_holder='ika'):
        print('Starting HYDRA reaction')
        self.kinova_deck_manager.solid_liquid_handling_manager.start_stirring_ika(300)


        # dose solvent 3ml
        self.kinova_deck_manager.dispense_solvent(volume=3,
                                                  solvent=self.kinova_deck_manager.
                                                  solid_liquid_handling_manager.VAPOURTEC_TO_THF,
                                                  vial_holder=vial_holder)
        print('adding THF')
        self.current_volume += 3


        # dose 0.1M IS 1ml
        self.kinova_deck_manager.dose_reagent(reagent_row=is_row, reagent_column=is_column,
                                              volume=1,
                                              vial_holder=vial_holder)
        self.current_volume += 1

        if self.INTERNAL_STANDARD_RT is None:
            self.hydra_sampling(vial_holder = vial_holder)
            time.sleep(600)
            identify_appeared_peak(self.hplc_folder, self.peak_times_and_labels, 'Internal Standard')
            print(
                'Internal standard is found at peak ' + str(self.peak_times_and_labels['Internal Standard']))

            print(self.peak_times_and_labels)

        # dose 0.5M acid 1ml first dose
        print('Adding acid')
        self.kinova_deck_manager.dose_reagent(reagent_row=acid_row, reagent_column=acid_column,
                                              vial_holder=vial_holder, volume=1)
        self.current_volume += 1
        first_volume_after_acid = self.current_volume

        # take 2 samples
        for i in range(2):
            self.hydra_sampling(vial_holder)

        # find ir and hplc folder
        self.ir_folder = update_for_ir_folder()
        print('The ir folder is ' + str(self.ir_folder))
        self.ir = IRAnalysisTools(self.ir_folder)
        self.hplc_folder = update_for_hplc_folder()
        print('The hplc folder is ' + str(self.hplc_folder))

        # Calculate ratio before and after sampling to account for actual remaining amount
        first_ratio = self.current_volume / first_volume_after_acid

        # second dosage of acid
        self.kinova_deck_manager.dose_reagent(reagent_row=acid_row, reagent_column=acid_column, vial_holder=vial_holder,
                                              volume=1 * first_ratio)
        self.current_volume += 1 * first_ratio
        second_volume_after_acid = self.current_volume

        for i in range(2):
            self.hydra_sampling(vial_holder)

        second_ratio = self.current_volume / second_volume_after_acid
        total_ratio = first_ratio * second_ratio

        # for more efficient reaction execution if we know internal standard beforehand
        if self.INTERNAL_STANDARD_RT is not None:
            label_one_peak(self.hplc_folder, self.peak_times_and_labels, 'Internal Standard', self.INTERNAL_STANDARD_RT)
        print(
            'Internal Standard is found at peak ' + str(self.peak_times_and_labels['Internal Standard']))

        print(self.peak_times_and_labels)
        identify_appeared_peak(self.hplc_folder, self.peak_times_and_labels, 'Acid')
        print(
            'Acid is found at peak ' + str(self.peak_times_and_labels['Acid']))
        print(self.peak_times_and_labels)

        # Dose 8mL of cdi
        cdi_volume = total_ratio * 8
        print('Adding CDI, CDI volume ' + str(cdi_volume))
        self.kinova_deck_manager.dose_reagent(vial_holder=vial_holder, volume=cdi_volume, is_cdi=True)
        self.current_volume += cdi_volume
        after_cdi_dosing_volume = self.current_volume
        print('CDI dose complete. Check for labels.')

        i = 30  # i is maximum number of samples
        while is_there_peak(self.hplc_folder, self.peak_times_and_labels['Acid']) and i >= 1:
            self.hydra_sampling(vial_holder)
            i -= 1
            print(i)


        identify_appeared_peak(self.hplc_folder, self.peak_times_and_labels, 'Intermediate')
        print(
            'Intermediate is found at peak ' + str(self.peak_times_and_labels['Intermediate']))

        # water addition (50uL)
        self.kinova_deck_manager.dose_reagent(reagent_row=water_row, reagent_column=water_column,
                                              volume=0.05)

        print('water dose done, waiting for ir confirmation')
        self.kinova_deck_manager.arm_manager.arm.home(wait=False)

        # ir-assisted cdi quench and n2 purging
        # need a time for collecting useful ir data
        wait_for_data_collection = 300 #s
        time.sleep(wait_for_data_collection)
        i = 0
        while (not self.ir.is_plateau('CDI', amount_of_interest=round(wait_for_data_collection / 60) - 1)) and i != 180:
            time.sleep(60)
            i += 1

        print('Quench done after ' + str(wait_for_data_collection / 60 + i) + ' min.')

        print('Starting N2 purge')
        self.kinova_deck_manager.nitrogen_purge(vial_holder=vial_holder, ir=self.ir)
        print('N2 purge complete')

        # SECOND STEP
        # Take 3 samples to HPLC for the initial points
        self.HPLC_TIME_INTERVAL = 600
        for i in range(2):
            self.hydra_sampling(vial_holder)

        # 1.0 M amine addition (1mL)
        amine_volume = 1 * self.current_volume / after_cdi_dosing_volume * total_ratio
        print('amine volume ' + str(amine_volume))
        self.kinova_deck_manager.dose_reagent(reagent_row=amine_row, reagent_column=amine_column,
                                              vial_holder=vial_holder, volume=amine_volume)


        # monitoring every 10 min. 65 sec interval means 10mins rxn monitoring interval
        for i in range(4):
            self.hydra_sampling(vial_holder)

        label_appeared_increasing_peak(self.hplc_folder, self.peak_times_and_labels, 'Amide', data_away_from_last=3)
        label_appeared_decreasing_peak(self.hplc_folder, self.peak_times_and_labels, 'Amine', data_away_from_last=3)
        print(
            'Amine is found at peak ' + str(self.peak_times_and_labels['Amine']))
        print(
            'Amide is found at peak ' + str(self.peak_times_and_labels['Amide']))
        print(str(self.peak_times_and_labels))

        for i in range(8):
            self.hydra_sampling(vial_holder)

        # adjust theoretical wait time for completing the reaction in 6 times of sampling
        self.HPLC_TIME_INTERVAL = get_wait_time_per_hplc_for_completion(self.hplc_folder, self.peak_times_and_labels
                                                                        , 'Intermediate', )
        print('Taking 6 samples with ' + str(self.HPLC_TIME_INTERVAL) + '-second interval.')

        i = 20 # max sampling
        while i >= 1 and (get_progress(self.hplc_folder, self.peak_times_and_labels, 'Amine') < 1):
            self.hydra_sampling(vial_holder)
            i -= 1

        # Take 3 samples to HPLC before standard addition
        self.HPLC_TIME_INTERVAL = 600
        for i in range(3):
            self.hydra_sampling(vial_holder)

        # 1M amine addition (0.1mL)
        print('current total volume ' + str(self.current_volume))
        print('adding amine')
        self.kinova_deck_manager.dose_reagent(reagent_row=amine_row, reagent_column=amine_column, volume=0.1)
        self.current_volume += 0.1
        print('current total volume ' + str(self.current_volume))

        # Take 3 samples to HPLC
        for i in range(3):
            self.hydra_sampling(vial_holder)

        # 1M amine addition (0.1mL)
        print('current total volume ' + str(self.current_volume))
        print('adding amine')
        self.kinova_deck_manager.dose_reagent(reagent_row=amine_row, reagent_column=amine_column, volume=0.1)
        self.current_volume += 0.1
        print('current total volume ' + str(self.current_volume))

        # Take 3 samples to HPLC
        for i in range(3):
            self.hydra_sampling(vial_holder)
        self.kinova_deck_manager.solid_liquid_handling_manager.stop_stirring_ika()
        print('done')

    # private method
    def hydra_sampling(self, vial_holder, report_to_slack: bool = True):
        self.kinova_deck_manager.take_hplc_sample(vial_holder=vial_holder, sample_volume=self.SAMPLE_VOLUME,
                                                  time_interval=self.HPLC_TIME_INTERVAL,
                                                  derivatization_setup=True)
        self.current_volume -= self.SAMPLE_VOLUME
        if report_to_slack:
            threading.Thread(target=self.visualization).start()

    # private method
    def visualization(self, delay=420):
        time.sleep(delay)
        plot_reaction_data_ratio(self.hplc_folder, self.peak_times_and_labels)
        bot.post_slack_file(os.path.join(self.hplc_folder, 'reaction_progress_ratio.pdf'), title='Ratio',
                              comment='Ratio Progress')
    # private method
    def initial_sample(self):
        before_interval = self.HPLC_TIME_INTERVAL
        self.HPLC_TIME_INTERVAL = None
        self.kinova_deck_manager.take_hplc_sample(vial_holder='ika', sample_volume=self.SAMPLE_VOLUME,
                                                  time_interval=self.HPLC_TIME_INTERVAL,
                                                  derivatization_setup=True)
        self.current_volume -= self.SAMPLE_VOLUME
        self.HPLC_TIME_INTERVAL = before_interval
        threading.Thread(target=self.visualization).start()


if __name__ == '__main__':
    hydra = HydraExperiment(kinova_deck_manager=kinova_deck_manager)
    try:
        hydra.run()
    except Exception as e:
        print(traceback.format_exc())
        print(sys.exc_info()[2])
    finally:
        turn_off_pump(480)
