# HPLC Analysis Tools



## Description
This project serves for the purpose of HPLC analysis. Analysis methods are used to extract, analysis, and visualize HPLC data. The program uses integration evaluation to determine the change of multiple species in a reaction mixture. It features peak labelling to scan and label new peaks for decision-making and data visualization.

## Installation
This program requires OpenLab LC Online for data collection. 

The requirements of this package can be found inside of requirements.txt. The specific version of the external libraries, which were used to test this program, are listed also. The versions of these libraries are not limited to those but reliable to run successfully.
## Usage

Many methods were built as private methods, which were helper methods to support general purposes of analysis and visualization. This introduction will only include the functionality of this program instead of the underlying algorithms that this program uses. The detailed description of this program can be found in the Hydra paper.

### Frequently Used Methods
A table of frequently used methods and the description is presented below:

| Method Name      | Description |
| ----------- | ----------- |
| experimentally_monitored_data |Scan the entire experiment folder. Get a list of retention times and a list of raw and ratio data associated with the retention times. |
| identify_appeared_peak      | Scan  the last chromatogram and label an unknown peak       |
| label_one_peak   | Assign a specific peak retention time a name        |
| label_appeared_increasing_peak   | Scan for the last few chromatograms and label an unknown and increasing peak      |
| label_appeared_decreasing_peak   | Scan for the last few chromatograms and label an unknown and decreasing peak        |
|is_there_peak|Scan the last chromatogram and return True if a given species is still detected|
| get_progress | Calculate a specific peak area's progress of being consumed (to zero). |
| plot_reaction_data_raw | Plot the labelled time course reaction progress data as raw integrations. |
| plot_reaction_data_ratio | Plot the labelled time course reaction progress data as  integrations referenced with the internal standard. |
| plot_reaction_data_concentration | Plot the labelled time course reaction progress data as concentrations (Hydra project).  |

### Arguments

For the methods to work properly and independently, they require many common inputs. A list of these inputs and the descriptions are presented below:

| Argument Name      | Description |
| ----------- | ----------- |
| folder | The folder that contains the core data files (txt/csv/acam_ files) |
| parent folder | The folder that contains the entire experiment folders |
| max_data_point_amount | The maximum amount of data to analyze |
| internal_standard_rt / is_rt | The internal standard retention time for peak ratio calculation |
| tolerance | The allowed peak shift from the target |
| labels/peaks | The collection of peak labels (See Analysis and Visualization) |
| target | One single target for analysis |


### Analysis and Visualization

For each chromatogram, the program scans for peaks and integrate to get the areas.
!['Chromatogram'](plots/chromatogram.png)

To analyze and visualize the HPLC data, knowing which retention time corresponding to which chemical is paramount. We use a dictionary object in Python to store the known peaks.
An example of this dictionary can look like this.
```
peak_labels_and_times = {'Internal Standard': 2.70895, 
                         'Acid': 2.07645, 
                         'Intermediate': 2.4152,
                         'Amide': 2.9243666666666667,
                         'Amine': 1.8085333333333333}
```

To determine the completion of a reaction, we used `is_there_peak` method to scan for the existence of a peak. If a peak becomes undetectable, the method returns `False` and the sequence moves on to the next step.

During the processing of the entire experiment, many chromatograms like this are analyzed. Each retention time with the integration is saved in a time series and visualized as a progress plot. Using the labels we can identify the corresponding names of the peak and visualize them as legends. 
!['Progress'](plots/progress.png)
<center>Time</center>
Furthermore, an GIF version of the entire progress of a reaction can be visualized:

!['ProgressGIF'](plots/progress_ratio.gif)


<center>Time</center>




## Contributing

No further contributions are sought at this moment.

## Authors and acknowledgment
The program is developed and tested by Fan (Luke) Yang at the Hein lab.


