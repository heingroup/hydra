import os
import pickle
from analysis_methods import get_last_experimental_data, experimentally_monitored_data
from ..watchers.watcher import Watcher
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


def is_there_peak_in_collection(retention_times: dict, peak_retention_time, shift_tolerance=0.04):
    """
    Find if a peak retention time is already in the given dictionary
    :param retention_times: collection of peaks
    :param peak_retention_time: one retention time in min
    :return: True if the retention time is already collected
    """
    for peaks in retention_times:
        if abs(retention_times[peaks] - peak_retention_time) <= shift_tolerance:
            return True
    return False


def is_there_peak(folder, peak, shift_tolerance=0.04):
    """
    Find if a given retention time has a peak
    :param folder: data folder
    :param peak: retention time of the peak in min
    :return: True if there is a peak
    """
    retention_times, _ = get_last_experimental_data(folder)
    for time in retention_times:
        if abs(peak - time) <= shift_tolerance:
            return True
    return False


def identify_appeared_peak(folder, peaks: dict, label: str, shift_tolerance=0.04):
    """
    Name one appearing peak with a given label,
    Use only exactly one new peak appeared
    :param peaks: collection of peaks
    :param label: name of the appeared peak
    :modifies the peaks
    """
    retention_time, ratio = get_last_experimental_data(folder)
    i = 0
    for time in retention_time:

        if not is_there_peak_in_collection(peaks, time, shift_tolerance=shift_tolerance) and ratio[i] > 0.25:
            peaks[label] = time
        i += 1


def label_one_peak(folder, peaks: dict, label: str, peak_retention_time, shift_tolerance=0.04):
    """
    :param folder: data folder
    :param peaks: collection of peaks
    :param label: name of the peak
    :param peak_retention_time: retention time of the peak
    :modifies the peaks
    """
    retention_time, _ = get_last_experimental_data(folder)
    for time in retention_time:
        if abs(peak_retention_time - time) <= shift_tolerance:
            peak_retention_time = time

    peaks[label] = peak_retention_time


def label_appeared_increasing_peak(folder, peaks: dict, label: str, data_away_from_last: int = 5):
    """
    :param folder: data folder
    :param peaks: collection of peaks
    :param label: name of the increasing peak
    :param data_away_from_last: consider how far from the analysis
    :modifies the peaks
    """
    distinct_peak_retention_time, time_point, peak_ratio, _ = experimentally_monitored_data(folder)
    for i in range(len(distinct_peak_retention_time)):
        current_peak = peak_ratio[i]
        if not is_there_peak_in_collection(peaks, distinct_peak_retention_time[i]):
            if current_peak[len(current_peak) - data_away_from_last - 1] - current_peak[len(current_peak) - 1] <= 0:
                peaks[label] = distinct_peak_retention_time[i]
                return peaks


def label_appeared_decreasing_peak(folder, peaks: dict, label: str, data_away_from_last: int = 5):
    """
    :param folder: data folder
    :param peaks: collection of peaks
    :param label: name of the decreasing peak
    :param data_away_from_last: consider how far from the analysis
    :modifies the peaks
    """
    distinct_peak_retention_time, time_point, peak_ratio, _ = experimentally_monitored_data(folder)
    for i in range(len(distinct_peak_retention_time)):
        current_peak = peak_ratio[i]
        if not is_there_peak_in_collection(peaks, distinct_peak_retention_time[i]):
            if current_peak[len(current_peak) - data_away_from_last - 1] - current_peak[len(current_peak) - 1] >= 0:
                peaks[label] = distinct_peak_retention_time[i]
                return


def get_progress(folder: str, peaks: dict, target: str, tolerance: float = 0.04):
    """
    get the percentage completion of the peak
    :param folder: data folder
    :param peaks: collection of peaks
    :param target: target peak for completion analysis
    :return: the completion of the progress [0,1]
    """
    distinct_peak_retention_time, _, peak_ratio, _ = experimentally_monitored_data(folder)
    progress = None
    for i in range(len(distinct_peak_retention_time)):
        if abs(peaks[target] - distinct_peak_retention_time[i]) <= tolerance:
            progress = peak_ratio[i]
            break

    if progress == None:
        print('Peak progress not found')
        return
    else:
        return (max(progress) - progress[-1]) / max(progress)


def get_wait_time_per_hplc_for_completion(folder: str,
                                          peaks: dict,
                                          target: str,
                                          tolerance=0.04,
                                          target_hplc_runs: int = 6):
    """
    get how long hplc should wait for completing the process in target_hplc_runs times
    :param folder: data folder
    :param peaks: collection of peaks
    :param target: target peak for analysis
    :param target_hplc_runs: expected finishing count
    :return: time interval for hplc wait
    """
    distinct_peak_retention_time, times, peak_ratio, _ = experimentally_monitored_data(folder)
    progress = None
    for i in range(len(distinct_peak_retention_time)):
        if abs(peaks[target] - distinct_peak_retention_time[i]) <= tolerance:
            progress = peak_ratio[i]
            break

    if progress is None:
        print('Peak progress not found')
        return
    else:
        progress = (max(progress) - progress[-1]) / max(progress)

    max_index = np.where('peak_ratio[i] == max(peak_ratio[i])')[0][0]
    time_consumed = (times[-1] - times[max_index]) * 3600

    left_time = (1 - progress) * time_consumed / progress
    interval = round((left_time / target_hplc_runs) / 100) * 100
    return max([interval, 600])


def update_for_hplc_folder():
    """
    watch for new appearing files so that the program knows the current
    hplc folder
    :return: the folder directory
    """
    w = Watcher()
    return w.run()


def save_json_file(json, file):
    # save the dictionary locally
    with open(file, 'wb') as fp:
        pickle.dump(json, fp, protocol=pickle.HIGHEST_PROTOCOL)


def load_json_file(file):
    # load the dictionary locally
    with open(file, 'rb') as fp:
        data = pickle.load(fp)
    return data


def plot_reaction_data_ratio(folder, labels, internal_standard_rt: float = None,
                             main_tolerance: float = 0.04,
                             save_plot: bool = True, save_data: bool = True, show_plot=False):
    """
    plot the data normalized by internal standard
    :param folder: data folder
    :param labels: collection of peaks
    :return: data frame for analysis
    """
    tol = main_tolerance
    times, time, ratio, _ = experimentally_monitored_data(folder, internal_standard_rt=internal_standard_rt,
                                                          peak_width=tol)
    cut = len(times)
    for i in range(len(times)):
        if is_there_peak_in_collection(labels, times[i], shift_tolerance=tol):
            times.append(times[i])
            ratio.append(ratio[i])

    times = times[cut:]
    ratio = ratio[cut:]
    for i in range(len(times)):
        min_len = min(len(time), len(ratio[i]))
        time = time[:min_len]
        ratio[i] = ratio[i][:min_len]
        plt.scatter(time, ratio[i])
    legends = []
    for t in times:
        for label in labels:
            if abs(labels[label] - t) <= main_tolerance:
                legends.append(label)
    plt.legend(legends)
    if save_plot:
        plt.savefig(os.path.join(folder, 'reaction_progress_ratio.pdf'))

    inverse_ratio = np.array(ratio).T.tolist()
    for i in range(len(inverse_ratio)):
        inverse_ratio[i].append(time[i])
    df1 = pd.DataFrame(inverse_ratio,
                       columns=legends+['Time'])
    if save_data:
        df1.to_excel(os.path.join(folder, 'progress_data_ratio_72.xlsx'))
    if show_plot:
        plt.show()
        plt.close()
    return df1


def plot_reaction_data_raw(folder, labels, main_tolerance: float = 0.04,
                           save_plot: bool = True, save_data: bool = True, show_plot=False):
    """
    plot the raw data
    :param folder: data folder
    :param labels: collection of peaks
    """
    tol = main_tolerance
    times, time, _, raw = experimentally_monitored_data(folder, peak_width=tol)
    cut = len(times)
    for i in range(len(times)):
        if is_there_peak_in_collection(labels, times[i], shift_tolerance=tol):
            times.append(times[i])
            raw.append(raw[i])

    times = times[cut:]
    raw = raw[cut:]
    for i in range(len(times)):
        plt.scatter(time, raw[i])
    legends = []
    for time in times:
        for label in labels:
            if abs(labels[label] - time) <= main_tolerance:
                legends.append(label)
    plt.legend(legends)
    if save_plot:
        plt.savefig(os.path.join(folder, 'reaction_progress_raw.pdf'))
    if save_data:
        inverse_ratio = np.array(raw).T.tolist()
        df1 = pd.DataFrame(inverse_ratio,
                           columns=legends)
        df1.to_excel(os.path.join(folder, 'progress_data_raw.xlsx'))
    if show_plot:
        plt.show()


def plot_reaction_data_concentration(folder, labels, main_tolerance=0.4, is_rt = 2.7,
                                     save_plot: bool = True, save_data: bool = True, show_plot=False):
    data = plot_reaction_data_ratio(folder, labels, is_rt,main_tolerance, save_plot, save_data, False)
    return data


if __name__ == '__main__':
    # print(get_wait_time_per_hplc_for_completion(r'D:\Chemstation\1\Data\2021-12-21\hydra_kinova 2021-12-21 14-22-37',
    #                    {'Internal Standard': 2.95, 'Acid': 2.0816666666666664,
    #                     'Intermediate': 2.4091666666666667, 'Amide': 3.19, 'Amine': 1.95}, 'Acid', ))
    is_there_peak(r'D:\Chemstation\1\Data\2021-12-21\hydra_kinova 2021-12-21 14-22-37')
