import os
import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
depth = 5
directory = ''
class Watcher:
    def __init__(self,directory_to_watch = r"D:\Chemstation\1\Data"):
        global directory
        directory = directory_to_watch
        self.observer = Observer()
        self.DIRECTORY_TO_WATCH = directory_to_watch

    def run(self):
        global depth, directory
        event_handler = Handler()
        self.observer.schedule(event_handler, self.DIRECTORY_TO_WATCH, recursive=True)
        self.observer.start()
        i = 3000
        try:
            while directory=='' and i >= 1:
                time.sleep(1)
                i -=1
        except:
            self.observer.stop()
            print("Error")

        self.observer.stop()
        return directory


class Handler(FileSystemEventHandler):
    @staticmethod
    def on_any_event(event):
        global directory
        if event.is_directory:
            return None

        elif event.event_type == 'created':
            try:

                directory = take_n_level_dir(event.src_path,5)
                print(directory)
            except:
                print('pass')

        elif event.event_type == 'modified':
            # Taken any action here when a file is modified.
            pass
        return directory
def take_n_level_dir(path:str,n:int):
    """
    works for mac system
    :param path:
    :param n:
    :return:
    """
    split = path.split('\\')
    x = split[n+1]
    join = '\\'.join(split[:n+1])
    return join


if __name__ == '__main__':
    w = Watcher()
    w.run()
