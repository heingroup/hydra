# IR Analysis Tools



## Description
This project serves for the purpose of IR analysis. A class instance is used to extract, analysis, and visualize IR data. The program uses integration or two-point-height evaluation to determine the change of a specific species in a reaction mixture. It uses plateau detection with self-determined threshold for decision-making.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
This program requires iC IR for data collection. Chronologically organized data can also be used if the format is the same as the sample data.

The requirements of this package can be found inside of requirements.txt. The specific version of the external libraries, which were used to test this program, are listed also. The versions of these libraries are not limited to those but reliable to run successfully.
## Usage

### Instantiation
Firstly, we need to instantiate a class for IR data. The default instantiation is
```
ir = IRAnalysisTools('path/to/myFolder')
```

where ```myFolder``` is the parent folder that contains all data files in CSV format.

### Analysis
When the class is instantiated, the folder is scanned and all the IR data are loaded. We can directly use methods built in the class to evaluate the data loaded in the class.

The user needs to pre-determine the corresponding regions of the desired chemical to monitor. Methods for analysis require an input named "target", which is the chemical to monitor. The format of the target input is ```[wavenumber_left, wavenumber_right].``` For example, if the peak of chemical A spans from 2000 wavenumber to 2010 wave number, we can set `target = [2000,2010].`

The class includes two default ranges of carbonyldiimidazole (CDI) and carbon dioxide (CO2), which were the primary targets for the Hydra project in the Hein lab. To conveniently use the method, we also allow for string inputs. A `COMMON_RANGES` variable of type `dict` is used to convert string inputs to IR ranges. In this case, `target = 'CO2` can be used to monitor the IR data of carbon dioxide. We can use 
```
ir = IRAnalysisTools('path/to/myFolder', targets = {new_target_name = [new_target_wavenumber_left, new_target_wavenumber_right]})
```
to update the frequently used targets.

We can use `get_time_course_height` or `get_time_course_integration` to calculate the   height or integration data of a target over time and to visualize the time-course data.
For example, in the previous experiments, we monitored the time-course data for CDI using
```
ir.get_time_course_height(target = 'CDI', plot = True)
```
to return

!['CDI'](plots/img.png).

We can use `is_plateau` to detect whether the target has reached its steady state. It has a preset threshold determined to work for data generated in the Hydra project. Since data may vary among instrument, this value may need to be adjusted for accurate plateau detection.


## Contributing

No further contributions are sought at this moment.

## Authors and acknowledgment
The program is developed and tested by Fan (Luke) Yang at the Hein lab.


