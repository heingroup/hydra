import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from experiments.utilities.hplc_analysis_tools.configuration import deck

depth = 3
class WatcherIR:
    def __init__(self,directory_to_watch = r"D:\iClC & iCIR data files\iC IR Experiments"):
        self.observer = Observer()
        self.DIRECTORY_TO_WATCH = directory_to_watch

    def find_ir_folder(self):
        event_handler = Handler()
        self.observer.schedule(event_handler, self.DIRECTORY_TO_WATCH, recursive=True)
        self.observer.start()
        i = 3000
        try:
            while deck.ir_folder== '' and i >= 1:
                time.sleep(1)
                i -=1
        except:
            self.observer.stop()
            print("Error")
        self.observer.stop()



class Handler(FileSystemEventHandler):
    @staticmethod
    def on_any_event(event):
        if event.is_directory:
            return None

        elif event.event_type == 'created':
            try:

                deck.ir_folder = take_n_level_dir(event.src_path, depth)
                print(deck.ir_folder)
            except:
                print('pass')

        elif event.event_type == 'modified':
            # Taken any action here when a file is modified.
            pass

def take_n_level_dir(path:str,n:int):
    """
    works for mac system
    :param path:
    :param n:
    :return:
    """
    split = path.split('\\')
    x = split[n+1]
    join = '\\'.join(split[:n+1])
    return join


if __name__ == '__main__':
    w = WatcherIR()
    w.find_ir_folder()