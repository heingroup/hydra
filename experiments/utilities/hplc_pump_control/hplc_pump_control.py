import warnings

import time

import pyautogui


def find_icon(icon: str, loop: int = 10):
    result = None
    for i in range(loop):
        result = pyautogui.locateOnScreen(icon)
        if result is not None:
            return result
    warnings.warn('Cannot find icon,'+ icon)
    return result


def open_and_maximize_lc(lc_icon_pos=(711, 1055)):
    lc_icon_pos = find_icon(r'D:\git_repositories\Direct_inject_N9\experiments\gold_nanoparticle\utilities\hplc_pump_control\lc_pin.png')
    pyautogui.click(lc_icon_pos)
    time.sleep(1)
    lc_online_pos = find_icon(r'D:\git_repositories\Direct_inject_N9\experiments\gold_nanoparticle\utilities\hplc_pump_control\lc_pop_up.png')
    pyautogui.click(lc_online_pos)
    time.sleep(1)
    response = find_icon(r'D:\git_repositories\Direct_inject_N9\experiments\gold_nanoparticle\utilities\hplc_pump_control\maximize.png')
    if response is not None:
        maximize_pos = (response[0] + 50,
                        response[1] + 10)
        pyautogui.click(maximize_pos)





# response = pyautogui.locateOnScreen()
def change_pump_status(turn_on=True):
    """
    Assume that the LC GUI is already opened and maximized
    """
    response = find_icon(r'D:\git_repositories\Direct_inject_N9\experiments\gold_nanoparticle\utilities\hplc_pump_control\pump.png')
    on_off_pose = (response[0] + 20,
                   response[1] + 150)
    pyautogui.moveTo(on_off_pose)
    time.sleep(1)
    response = find_icon(r'D:\git_repositories\Direct_inject_N9\experiments\gold_nanoparticle\utilities\hplc_pump_control\pump_on_off.png')

    if response is None:
        raise Exception('Cannot find the pump icon')
    on_pos = (response[0] + 13, response[1] + 8)
    off_pos = (response[0] + 48, response[1] + 8)
    time.sleep(1)
    if turn_on:
        pyautogui.moveTo(on_pos)
        time.sleep(1)
        pyautogui.click()

        time.sleep(1)
        pyautogui.click()
    else:
        pyautogui.moveTo(off_pos)
        pyautogui.click()

        time.sleep(1)
        pyautogui.click()

def turn_on_pump(delay = 0):
    time.sleep(delay)
    open_and_maximize_lc()
    change_pump_status(True)

def turn_off_pump(delay = 0):
    time.sleep(delay)
    open_and_maximize_lc()
    change_pump_status(False)
if __name__ == '__main__':
    # turn_on_pump()
    # time.sleep(5)
    turn_on_pump()