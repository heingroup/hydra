import sys
import traceback

from configuration.config import kinova_deck_manager
from configuration.hydra_experiment.hydra_experiment import HydraExperiment
from experiments.utilities.hplc_pump_control.hplc_pump_control import turn_off_pump

hydra = HydraExperiment(kinova_deck_manager=kinova_deck_manager)
try:
    hydra.run()
except Exception as e:
    print(traceback.format_exc())
    print(sys.exc_info()[2])
finally:
    turn_off_pump(480)