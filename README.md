# Hydra Project via Kinova Reaction Platform



## Introduction

The program is for the Hein Lab project Hydra. It includes both the instrument
and algorithm code we built for completing the reaction monitoring. The code mainly serves
 demonstration purposes. Plans to adapt this code to your own system may require
the purchase of the identical modules on the Kinova deck, adjustment of the locations of
the robotic arm, and extensive assistance from the Hein Lab.

## Description


- Configuration
  -  Integrated control of all instruments. Including connection establishments, arm location records, and sequential actions for experimental tasks.|
- Experiment
  - Actual execution of an experiment. Including tools for IR analysis, HPLC analysis, and PyAutoGUI-assisted Agilent software control.
  
## Installation
Please contact Hein Lab for assistance on the installation. For HPLC and IR analysis only, regular installations of SciPy, NumPy, and Pandas are sufficient.

## Video Demonstration

Here we present a video of the running platform. This video illustrates the robotic movements needed to complete the reaction execution and monitoring.


[![Amide Coupling](https://i3.ytimg.com/vi/NNY_mhC-roE/maxresdefault.jpg)](https://youtu.be/NNY_mhC-roE "Amide coupling")


## Contributing
Currently, we do not expect further contributions because of the distinctiveness of each platform.

## Authors and acknowledgment

This program is written by Fan (Luke) Yang. 

Synthetic steps are provided by Junliang Liu and Yusuke Sato. 

Acknowledgments to Sean Clark, Henry Situ, Veronica Lai, Lars Yunker for instrumental control and data decoding.

